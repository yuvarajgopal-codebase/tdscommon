package com.tdsecurities.common.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.tdsecurities.common.auth.AuthenticationFacade;
import com.tdsecurities.common.data.ActiveImage;
import com.tdsecurities.common.data.ActiveImageRepository;
import com.tdsecurities.common.data.Image;
import com.tdsecurities.common.data.ImageId;
import com.tdsecurities.common.data.ImageMetadata;
import com.tdsecurities.common.data.ImageRepository;
import com.tdsecurities.common.data.ImageSize;
import com.tdsecurities.common.data.WorkflowStatus;
import com.tdsecurities.common.dto.ImageDTO;
import com.tdsecurities.common.dto.ImageMetadataDTO;
import com.tdsecurities.common.exception.ItemExistsException;
import com.tdsecurities.common.exception.ItemNotFoundException;
import com.tdsecurities.common.util.TransformUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ImageService {

    private static final Logger logger = LoggerFactory.getLogger(ImageService.class);

    @Autowired
    ImageRepository imageRepository;

    @Autowired
    ActiveImageRepository activeImageRepository;

    @Autowired
    AuthenticationFacade authenticationFacade;

    /**
     * Saves a new image (or set of images) to the database
     * 
     * @param imageId
     * @param mimeType
     * @param defaultImage
     * @param notes
     * @param imageXs
     * @param imageXsFilename
     * @param imageSm
     * @param imageSmFilename
     * @param imageMd
     * @param imageMdFilename
     * @param imageLg
     * @param imageLgFilename
     * @return the metadata for the saved image
     * @throws ItemExistsException if an image aldready exists with the imageId
     */
    @Transactional
    public ImageMetadataDTO saveSingleImage(String imageId, String mimeType, String notes, 
        byte[] imageData, String imageFilename)  {

        // Check if image ID already exists
        if (imageRepository.existsById(new ImageId(imageId, 1))) {
            logger.warn("Cannot create image. Image with ID {} and version {} already exists", imageId, 1);
            throw new ItemExistsException("Image already exists");
        }
        
        // Create Image entity to save
        logger.info("Preparing to save image data");

        Image image = new Image();
        image.setId(imageId);
        image.setVersion(1);
        image.setMimeType(mimeType);
        image.setDefaultSize(ImageSize.md);
        image.setImageMdExists(true);
        image.setNotes(notes);
        image.setStatus(WorkflowStatus.Draft);
        image.setImageMd(imageData);
        image.setImageMdFilename(imageFilename);
        image.setCreatedAt(new Date());
        image.setCreatedBy(authenticationFacade.getAuthenticatedUserName());
        Image createdImage = imageRepository.save(image);

        // Save new ActiveImage to the database
        ActiveImage activePage = new ActiveImage();
        activePage.setImageId(imageId);
        activePage.setLiveVersion(0);
        activePage.setStageVersion(0);
        activePage.setCreatedAt(new Date());
        activePage.setCreatedBy(authenticationFacade.getAuthenticatedUserName());
        activeImageRepository.save(activePage);

        // Return DTO representing created image
        ImageMetadataDTO createdImageDTO = TransformUtils.getMetadataForImage(createdImage);

        logger.info("Image data saved");

        return createdImageDTO;
    }

    /**
     * Saves a new image (or set of images) to the database
     * 
     * @param imageId
     * @param mimeType
     * @param defaultImage
     * @param notes
     * @param imageXs
     * @param imageXsFilename
     * @param imageSm
     * @param imageSmFilename
     * @param imageMd
     * @param imageMdFilename
     * @param imageLg
     * @param imageLgFilename
     * @return the metadata for the saved image
     * @throws ItemExistsException if an image aldready exists with the imageId
     */
    @Transactional
    public ImageMetadataDTO saveMultiImage(String imageId, String mimeType,  ImageSize defaultSize, String notes, 
        byte[] imageXs, String imageXsFilename, byte[] imageSm, String imageSmFilename, byte[] imageMd,
        String imageMdFilename, byte[] imageLg, String imageLgFilename)  {

        // Check if image ID already exists
        if (imageRepository.existsById(new ImageId(imageId, 1))) {
            logger.warn("Cannot create image. Image with ID {} and version {} already exists", imageId, 1);
            throw new ItemExistsException("Image already exists");
        }
        
        // Create Image entity to save
        logger.info("Preparing to save image data");

        Image image = new Image();
        image.setId(imageId);
        image.setVersion(1);
        image.setMimeType(mimeType);
        image.setDefaultSize(defaultSize);
        image.setNotes(notes);
        image.setStatus(WorkflowStatus.Draft);
        image.setImageXs(imageXs);
        image.setImageXsFilename(imageXsFilename);
        image.setImageXsExists(imageXs != null ? true : false);
        image.setImageSm(imageSm);
        image.setImageSmFilename(imageSmFilename);
        image.setImageSmExists(imageXs != null ? true : false);
        image.setImageMd(imageMd);
        image.setImageMdFilename(imageMdFilename);
        image.setImageMdExists(imageXs != null ? true : false);
        image.setImageLg(imageLg);
        image.setImageLgFilename(imageLgFilename);
        image.setImageLgExists(imageXs != null ? true : false);
        image.setCreatedAt(new Date());
        image.setCreatedBy(authenticationFacade.getAuthenticatedUserName());
        Image createdImage = imageRepository.save(image);

        // Save new ActiveImage to the database
        ActiveImage activePage = new ActiveImage();
        activePage.setImageId(imageId);
        activePage.setLiveVersion(0);
        activePage.setStageVersion(0);
        activePage.setCreatedAt(new Date());
        activePage.setCreatedBy(authenticationFacade.getAuthenticatedUserName());
        activeImageRepository.save(activePage);

        // Return DTO representing created image
        ImageMetadataDTO createdImageDTO = TransformUtils.getMetadataForImage(createdImage);

        logger.info("Image data saved");

        return createdImageDTO;
    }

    /**
     * Get the metadata for the live version of an image
     * 
     * @param imageId
     * @return the metadata for the live version of the saved 
     * @throws ItemNotFoundException if the image with the imageId and version was not found or has no live version
     */
    public ImageMetadataDTO getLiveImageMetadata(String imageId) {

        if (imageId == null) {
            throw new IllegalArgumentException("Image ID is invalid");
        }

        Optional<ActiveImage> activeImageOptional = activeImageRepository.findById(imageId);

        if (!activeImageOptional.isPresent()) {
            logger.warn("Cound not find Image {}", imageId);
            throw new ItemNotFoundException("Could not find image metadata");
        }

        ActiveImage activeImage = activeImageOptional.get();
        if (activeImage.getLiveVersion() == 0) {
            logger.warn("No live version of image metadata {} found", imageId);
            throw new ItemNotFoundException("Could not find the live version of the image metadata");        
        }

        return getImageMetadata(imageId, activeImage.getLiveVersion());
    }

    /**
     * Get's the metadata for an image
     * 
     * @param imageId
     * @param version
     * @return the metadata for the saved image
     * @throws IllegalArgumentException if the input data was invalid or missing
     * @throws ItemNotFoundException if the image with the imageId and version was not found
     */
    public ImageMetadataDTO getImageMetadata(String imageId, long version) {

        if (imageId == null || version == 0) {
            throw new IllegalArgumentException("Image ID or version is invalid");
        }

        Optional<Image> imageOptional = imageRepository.findById(new ImageId(imageId, version));

        if (!imageOptional.isPresent()) {
            logger.warn("Cound not find Image {} with Version {}", imageId, version);
            throw new ItemNotFoundException("Could not find image metadata");
        }

        Image image = imageOptional.get();
        logger.info("Found Image {}", image.getId());

        ImageMetadataDTO imageMetadataDTO = TransformUtils.getMetadataForImage(image);
        return imageMetadataDTO;
    }

    /**
     * Returns the default image
     * 
     * @param imageId
     * @param version
     * @return A DTO with image information including the binary image data
     * @throws IllegalArgumentException if the input data was bad
     * @throws ItemNotFoundException if the image with imageId was not found
     */
    public ImageDTO getDefaultImage(String imageId, long version) {

        if (imageId == null) {
            throw new IllegalArgumentException("Image ID or version is invalid");
        }

        Optional<Image> imageOptional = imageRepository.findById(new ImageId(imageId, version));

        if (!imageOptional.isPresent()) {
            logger.warn("Cound not find Image {} with Version {}", imageId, version);
            throw new ItemNotFoundException("Could not find image");
        }

        Image image = imageOptional.get();
        logger.info("Found Image {}", image.getId());

        ImageDTO imageDTO = getImageForSize(image, image.getDefaultSize());
            
        return imageDTO;
    }

    /**
     * Returns the image for the size specified
     * 
     * @param imageId
     * @param version
     * @param imageSize
     * @return A DTO with image information including the binary image data
     * @throws IllegalArgumentException if the input data was bad
     * @throws ItemNotFoundException if the image with imageId was not found
     */
    public ImageDTO getImage(String imageId, long version, ImageSize imageSize) {

        if (imageId == null) {
            throw new IllegalArgumentException("Image ID or version is invalid");
        }

        Optional<Image> imageOptional = imageRepository.findById(new ImageId(imageId, version));

        if (!imageOptional.isPresent()) {
            logger.warn("Cound not find Image {} with Version {}", imageId, version);
            throw new ItemNotFoundException("Could not find image");
        }

        Image image = imageOptional.get();
        logger.info("Found Image {}", image.getId());

        ImageDTO imageDTO = getImageForSize(image, imageSize);
        
        // If no image data for the specified size, return the data for the default size
        if (imageDTO == null) {
            imageDTO = getImageForSize(image, image.getDefaultSize());
        }
        
        return imageDTO;
    }

    private ImageDTO getImageForSize(Image image, ImageSize imageSize) {

        if (image == null || imageSize == null) {
            return null;
        }

        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setId(image.getId());
        imageDTO.setVersion(image.getVersion());
        imageDTO.setMimeType(image.getMimeType());

        switch (imageSize) {
            case xs:
                imageDTO.setImageData(image.getImageXs());
                imageDTO.setSize(ImageSize.xs);
                break;
            case sm:
                imageDTO.setImageData(image.getImageSm());
                imageDTO.setSize(ImageSize.sm);
                break;
            case md:
                imageDTO.setImageData(image.getImageMd());
                imageDTO.setSize(ImageSize.md);
                break;
            case lg:
                imageDTO.setImageData(image.getImageLg());
                imageDTO.setSize(ImageSize.lg);
                break;
            default:
                return null;
        }

        return imageDTO;
    }


    public List<ImageMetadataDTO> getImageMetadataForAllImages() {

        List<ImageMetadata> imageMetadataList = imageRepository.findAllImageMetadata();

        if (imageMetadataList == null || imageMetadataList.size() == 0) {
            throw new ItemNotFoundException("No images found");
        }
  
        List<ImageMetadataDTO> imageMetadataDtoList = TransformUtils.xformImageMetadata(imageMetadataList);
        return imageMetadataDtoList;
    }
}