package com.tdsecurities.common.auth;

import org.springframework.stereotype.Component;

@Component
public class DummyAuthenticationFacade implements AuthenticationFacade {

    public String getAuthenticatedUserName() {
        return "Mr User";
    }
}