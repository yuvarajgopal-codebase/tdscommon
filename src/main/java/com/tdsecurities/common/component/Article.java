package com.tdsecurities.common.component;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import com.tdsecurities.common.config.CommonConstants;
import com.tdsecurities.common.dto.ArticleDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class Article implements BaseComponent {

    private static final Logger logger = LoggerFactory.getLogger(Article.class);

    private ArticleDTO articleDTO;
    private CommonConstants commonConstants;

    public Article(ArticleDTO articleDTO, CommonConstants commonConstants) {
        this.articleDTO = articleDTO;
        this.commonConstants = commonConstants;
    }

    @Override
    public String generateContent(Configuration freeMarkerConfiguration) {

        try {

            Map<String, Object> templateParams = new HashMap<>();

            Template template = freeMarkerConfiguration.getTemplate("component/article.ftl");
            templateParams.put("article", articleDTO);
            templateParams.put("constants", commonConstants);

            StringWriter stringWriter = new StringWriter();
            template.process(templateParams, stringWriter);

            return stringWriter.toString();

        } catch (Exception e) {
            logger.error("Error generating article content: ", e);
            throw new RuntimeException("Error generating article content", e);
        }
        
	}

}