package com.tdsecurities.common.component;

import freemarker.template.Configuration;

public interface BaseComponent {

    String generateContent(Configuration freeMarkerConfiguration);
    
}