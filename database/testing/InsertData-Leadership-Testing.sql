/* Data for the Leadership Page (for testing) */
INSERT INTO tdspw_template_info (id, version, template_file, active, created_at, created_by) 
    VALUES ('Leadership', 1, 'leadership', 1, CURRENT_TIMESTAMP, 'Script');
INSERT INTO tdspw_template_info (id, version, template_file, active, created_at, created_by) 
    VALUES ('Overview', 1, 'overview', 1, CURRENT_TIMESTAMP, 'Script');
INSERT INTO tdspw_template_info (id, version, template_file, active, created_at, created_by) 
    VALUES ('Insight', 1, 'insight', 1, CURRENT_TIMESTAMP, 'Script');
INSERT INTO tdspw_template_info (id, version, template_file, active, created_at, created_by) 
    VALUES ('Product', 1, 'product', 1, CURRENT_TIMESTAMP, 'Script');

INSERT INTO tdspw_template_component (template_id, template_version, sort_order, name, type, description, created_at, created_by)
VALUES ('Leadership', 1, 1, 'leadershipBanner', 'Banner', 'Leadership banner', CURRENT_TIMESTAMP, 'Script');

INSERT INTO tdspw_template_component (template_id, template_version, sort_order, name, type, description, created_at, created_by)
VALUES ('Leadership', 1, 2, 'leadershipGrid', 'LeadershipGrid', 'Leadership grid', CURRENT_TIMESTAMP, 'Script');

INSERT INTO tdspw_template_component (template_id, template_version, sort_order, name, type, description, created_at, created_by)
VALUES ('Leadership', 1, 3, 'fromLocalToGlobal', 'TextWithButtons', 'Find TD Securities team', CURRENT_TIMESTAMP, 'Script');

INSERT INTO tdspw_template_component (template_id, template_version, sort_order, name, type, description, created_at, created_by)
VALUES ('Leadership', 1, 4, 'articleList', 'ArticleList', 'List of articles', CURRENT_TIMESTAMP, 'Script');

INSERT INTO tdspw_template_component (template_id, template_version, sort_order, name, type, description, created_at, created_by)
VALUES ('Overview', 1, 1, 'overviewBanner', 'Banner', 'Overview banner', CURRENT_TIMESTAMP, 'Script');

INSERT INTO tdspw_template_component (template_id, template_version, sort_order, name, type, description, created_at, created_by)
VALUES ('Overview', 1, 2, 'articleList', 'ArticleList', 'List of articles', CURRENT_TIMESTAMP, 'Script');



INSERT INTO tdspw_template_component (template_id, template_version, sort_order, name, type, description, created_at, created_by)
VALUES ('Insight', 1, 1, 'insightBanner', 'Banner', 'Insight banner', CURRENT_TIMESTAMP, 'Script');

INSERT INTO tdspw_template_component (template_id, template_version, sort_order, name, type, description, created_at, created_by)
VALUES ('Insight', 1, 2, 'articleList', 'ArticleList', 'List of articles', CURRENT_TIMESTAMP, 'Script');

INSERT INTO tdspw_template_component (template_id, template_version, sort_order, name, type, description, created_at, created_by)
VALUES ('Product', 1, 1, 'imageCopy', 'ImageCopy', 'Image Copy', CURRENT_TIMESTAMP, 'Script');

INSERT INTO tdspw_template_component (template_id, template_version, sort_order, name, type, description, created_at, created_by)
VALUES ('Product', 1, 2, 'TabText', 'TabText', 'Tab Text', CURRENT_TIMESTAMP, 'Script');


-- page_info, page_data, can be either created using sql, or using postman and the backend endpoints.



--- 

INSERT INTO tdspw_page_info (id, version, template_id, template_version, notes, status, active, created_at, created_by) 
VALUES ('AboutUsLeadership', 1, 'Leadership', 1, 'Main page for About Us -> Leadership','Published', 1, CURRENT_TIMESTAMP, 'Script');

INSERT INTO tdspw_page_data (page_id, page_version, language, title, analytics_tag, seo_metadata, component_data, created_at, created_by) 
VALUES ('AboutUsLeadership', 1, 'en_CA', 'Our Executive Team', 'page-id-123456-en', 'td leadership executive team', NULL, CURRENT_TIMESTAMP, 'Script');

INSERT INTO tdspw_page_data (page_id, page_version, language, title, analytics_tag, seo_metadata, component_data, created_at, created_by) 
VALUES ('AboutUsLeadership', 1, 'fr_CA', 'Notre Equipe Executive', 'page-id-123456-fr', 'td leadership executive team', NULL, CURRENT_TIMESTAMP, 'Script');

UPDATE tdspw_page_data
SET component_data = '{"articleList":{"title":"Discover new insight on markets everyday","articles":[{"imageId":"ArticleImage1","imageAlt":"Article image","articleType":"LinkedArticle","articlePageId":"Article1Page","title":"How is your Ontario auto insurance premium determined","dateLabel":"Published: ","date":"20/06/2017","description":"Here\u0027s where you get the inside track on basic as well as optional coverages."}]},"leadershipGrid":{"title":"Our Executive Team","subtitle":"At TD Securities, our competitive advantage lies with our people, including the strength of our seniour executive group.","profiles":[{"imageId":"BobDorrance","imageAlt":"Bob Dorrance image","name":"Bob Dorrance","title":"CEO","description":"Chairmain and CEO","profilePageId":"ProfileBobDorrance"},{"imageId":"BobDorrance","imageAlt":"Bob Dorrance image","name":"Bob Dorrance","title":"CEO","description":"Chairmain and CEO","profilePageId":"ProfileBobDorrance"}]},"leadershipBanner":{"title":"Leadership banner","imageId":"LeadershipBanner","description":"Partner with seasoned experts the industry respects"}}'
WHERE page_id = 'AboutUsLeadership' AND page_version = 1 AND language = 'en_CA'

UPDATE tdspw_page_data
SET component_data = '{"articleList":{"title":"FRENCH Discover new insight on markets everyday","articles":[{"imageId":"ArticleImage1","imageAlt":"Article image","articleType":"LinkedArticle","articlePageId":"Article1Page","title":"FRENCH How is your Ontario auto insurance premium determined","dateLabel":"Published: ","date":"20/06/2017","description":"Here\u0027s where you get the inside track on basic as well as optional coverages."}]},"leadershipGrid":{"title":"Our FRENCH Executive Team","subtitle":"At TD Securities, our competitive advantage lies with our people, including the strength of our seniour executive group.","profiles":[{"imageId":"BobDorrance","imageAlt":"Bob Dorrance image","name":"Bob Dorrance","title":"CEO","description":"Chairmain and CEO","profilePageId":"ProfileBobDorrance"},{"imageId":"BobDorrance","imageAlt":"Bob Dorrance image","name":"Bob Dorrance","title":"CEO","description":"Chairmain and CEO","profilePageId":"ProfileBobDorrance"}]},"leadershipBanner":{"title":"Leadership banner","imageId":"LeadershipBanner","description":"FRENCH Partner with seasoned experts the industry respects"}}'
WHERE page_id = 'AboutUsLeadership' AND page_version = 1 AND language = 'fr_CA'

INSERT INTO tdspw_page_info (id, version, template_id, template_version, notes, status, active, created_at, created_by) 
VALUES ('AboutUsOverview', 1, 'Overview', 1, 'Overview page for About Us','Published', 1, CURRENT_TIMESTAMP, 'Script');

INSERT INTO tdspw_page_info (id, version, template_id, template_version, notes, status, active, created_at, created_by) 
VALUES ('InsightsOverview', 1, 'Overview', 1, 'Main page for Insights','Published', 1, CURRENT_TIMESTAMP, 'Script');

INSERT INTO tdspw_page_info (id, version, template_id, template_version, notes, status, active, created_at, created_by) 
VALUES ('CommunityUnderwritingHope', 1, 'Overview', 1, 'Underwriting Hope page under Community','Published', 1, CURRENT_TIMESTAMP, 'Script');

---

INSERT INTO tdspw_active_page (page_id, live_version, stage_version, created_at, created_by) 
VALUES ('AboutUsLeadership', 1, 1, CURRENT_TIMESTAMP, 'Script');

INSERT INTO tdspw_active_page (page_id, live_version, stage_version, created_at, created_by) 
VALUES ('AboutUsOverview', 1, 1, CURRENT_TIMESTAMP, 'Script');
