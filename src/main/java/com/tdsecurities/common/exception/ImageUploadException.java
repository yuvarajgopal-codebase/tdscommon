package com.tdsecurities.common.exception;

public class ImageUploadException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ImageUploadException() {
    }

    public ImageUploadException(String message) {
        super(message);
    }

    public ImageUploadException(Throwable cause) {
        super(cause);
    }

    public ImageUploadException(String message, Throwable cause) {
        super(message, cause);
    }
 
}