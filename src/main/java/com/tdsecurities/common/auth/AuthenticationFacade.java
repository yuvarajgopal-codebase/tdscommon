package com.tdsecurities.common.auth;

public interface AuthenticationFacade {
    String getAuthenticatedUserName();
}