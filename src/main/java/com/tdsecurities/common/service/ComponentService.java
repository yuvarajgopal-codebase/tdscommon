package com.tdsecurities.common.service;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.tdsecurities.common.component.ArticleList;
import com.tdsecurities.common.component.Banner;
import com.tdsecurities.common.component.ComponentType;
import com.tdsecurities.common.component.ImageCopy;
import com.tdsecurities.common.component.LeadershipGrid;
import com.tdsecurities.common.component.SimpleText;
import com.tdsecurities.common.component.TabText;
import com.tdsecurities.common.component.TextWithButtons;
import com.tdsecurities.common.config.CommonConstants;
import com.tdsecurities.common.dto.ArticleListDTO;
import com.tdsecurities.common.dto.BannerDTO;
import com.tdsecurities.common.dto.ImageCopyDTO;
import com.tdsecurities.common.dto.LeadershipGridDTO;
import com.tdsecurities.common.dto.TabTextDTO;
import com.tdsecurities.common.dto.TextWithButtonsDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import freemarker.template.Configuration;

@Service
public class ComponentService {

    private static final Logger logger = LoggerFactory.getLogger(ComponentService.class);

    @Autowired
    CommonConstants commonConstants;

    @Autowired
    Configuration freeMarkerConfiguration;

    public String generateContentForComponent(ComponentType componentType, JsonElement jsonElement) {

        if (componentType == null) {
            logger.warn("Attempt to generate content for null component type");
            return null;
        }

        logger.info("Component type is {}", componentType.name());

        switch (componentType) {
        case SimpleText:
            if (jsonElement.isJsonPrimitive()) {
                SimpleText simpleText = new SimpleText(jsonElement.getAsString());
                return simpleText.generateContent(freeMarkerConfiguration);
            } else {
                logger.warn("Expected string but got object for JSON element");
            }
            break;

        case Banner:
            if (jsonElement.isJsonObject()) {
                BannerDTO bannerDTO = new Gson().fromJson(jsonElement, BannerDTO.class);
                Banner banner = new Banner(bannerDTO, commonConstants);
                return banner.generateContent(freeMarkerConfiguration);
            } else {
                logger.warn("Expected object but got primitive for JSON element");
            }
            break;

        case LeadershipGrid:
            if (jsonElement.isJsonObject()) {
                LeadershipGridDTO leadershipGridDTO = new Gson().fromJson(jsonElement, LeadershipGridDTO.class);
                LeadershipGrid leadershipGrid = new LeadershipGrid(leadershipGridDTO, commonConstants);
                return leadershipGrid.generateContent(freeMarkerConfiguration);
            } else {
                logger.warn("Expected object but got primitive for JSON element");
            }
            break;

        case TextWithButtons:
            if (jsonElement.isJsonObject()) {
                TextWithButtonsDTO textWithButtonsDTO = new Gson().fromJson(jsonElement, TextWithButtonsDTO.class);
                TextWithButtons textWithButtons = new TextWithButtons(textWithButtonsDTO, commonConstants);
                return textWithButtons.generateContent(freeMarkerConfiguration);
            } else {
                logger.warn("Expected object but got primitive for JSON element");
            }
            break;

        case ArticleList:
            if (jsonElement.isJsonObject()) {
                ArticleListDTO articleListDTO = new Gson().fromJson(jsonElement, ArticleListDTO.class);
                ArticleList articleList = new ArticleList(articleListDTO, commonConstants);
                return articleList.generateContent(freeMarkerConfiguration);
            } else {
                logger.warn("Expected object but got primitive for JSON element");
            }
            break;

        case ImageCopy:
            if (jsonElement.isJsonObject()) {
                ImageCopyDTO imageCopyDTO = new Gson().fromJson(jsonElement, ImageCopyDTO.class);
                ImageCopy imageCopy = new ImageCopy(imageCopyDTO, commonConstants);
                return imageCopy.generateContent(freeMarkerConfiguration);
            } else {
                logger.warn("Expected object but got primitive for JSON element");
            }
            break;

        case TabText:
            if (jsonElement.isJsonObject()) {
                TabTextDTO tabTextDTO = new Gson().fromJson(jsonElement, TabTextDTO.class);
                TabText tabText = new TabText(tabTextDTO, commonConstants);
                return tabText.generateContent(freeMarkerConfiguration);
            } else {
                logger.warn("Expected object but got primitive for JSON element");
            }
            break;

        default:
            logger.warn("No handling for component type {}", componentType);
            return null;
        }

        return null;

    }

    public Object getComponentFromJson(ComponentType componentType, JsonElement jsonElement) {

        if (componentType == null) {
            logger.warn("Attempt to get component for null component type");
            return null;
        }

        logger.info("Component type is {}", componentType.name());

        switch (componentType) {
        case SimpleText:
            if (jsonElement.isJsonPrimitive()) {
                return jsonElement.getAsString();
            } else {
                logger.warn("Expected string but got object for JSON element");
            }
            break;

        case Banner:
            if (jsonElement.isJsonObject()) {
                BannerDTO bannerDTO = new Gson().fromJson(jsonElement, BannerDTO.class);
                return bannerDTO;
            } else {
                logger.warn("Expected object but got primitive for JSON element");
            }
            break;

        case LeadershipGrid:
            if (jsonElement.isJsonObject()) {
                LeadershipGridDTO leadershipGridDTO = new Gson().fromJson(jsonElement, LeadershipGridDTO.class);
                return leadershipGridDTO;
            } else {
                logger.warn("Expected object but got primitive for JSON element");
            }
            break;

        case TextWithButtons:
            if (jsonElement.isJsonObject()) {
                TextWithButtonsDTO textWithButtonsDTO = new Gson().fromJson(jsonElement, TextWithButtonsDTO.class);
                return textWithButtonsDTO;
            } else {
                logger.warn("Expected object but got primitive for JSON element");
            }
            break;

        case ArticleList:
            if (jsonElement.isJsonObject()) {
                ArticleListDTO articleListDTO = new Gson().fromJson(jsonElement, ArticleListDTO.class);
                return articleListDTO;
            } else {
                logger.warn("Expected object but got primitive for JSON element");
            }
            break;

        default:
            logger.warn("No handling for component type {}", componentType);
            return null;
        }

        return null;

    }

}