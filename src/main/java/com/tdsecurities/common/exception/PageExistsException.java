package com.tdsecurities.common.exception;

public class PageExistsException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public PageExistsException() {
    }

    public PageExistsException(String message) {
        super(message);
    }

    public PageExistsException(Throwable cause) {
        super(cause);
    }

    public PageExistsException(String message, Throwable cause) {
        super(message, cause);
    }

}