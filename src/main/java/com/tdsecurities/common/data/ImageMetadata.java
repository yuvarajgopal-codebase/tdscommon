package com.tdsecurities.common.data;

public interface ImageMetadata {

    String getId();
    long getVersion();
    String getMimeType();
    ImageSize getDefaultSize();
    String getNotes();
    WorkflowStatus getStatus();
    boolean isImageXsExists();
    boolean isImageSmExists();
    boolean isImageMdExists();
    boolean isImageLgExists();
}