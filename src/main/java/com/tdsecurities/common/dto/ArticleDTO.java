package com.tdsecurities.common.dto;

import com.tdsecurities.common.component.ArticleType;

public class ArticleDTO {

    private String imageId;
    private String imageAlt;
    private ArticleType articleType;
    private String articlePageId; // TODO: add link info to article page
    private String title;
    private String dateLabel;
    private String date;
    private String description;

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImageAlt() {
        return imageAlt;
    }

    public void setImageAlt(String imageAlt) {
        this.imageAlt = imageAlt;
    }

    public ArticleType getArticleType() {
        return articleType;
    }

    public void setArticleType(ArticleType articleType) {
        this.articleType = articleType;
    }
    
    public String getArticlePageId() {
        return articlePageId;
    }

    public void setArticlePageId(String articlePageId) {
        this.articlePageId = articlePageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateLabel() {
        return dateLabel;
    }

    public void setDateLabel(String dateLabel) {
        this.dateLabel = dateLabel;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ArticleDTO [articlePageId=" + articlePageId + ", articleType=" + articleType + ", date=" + date
                + ", dateLabel=" + dateLabel + ", description=" + description + ", imageAlt=" + imageAlt + ", imageId="
                + imageId + ", title=" + title + "]";
    }

}