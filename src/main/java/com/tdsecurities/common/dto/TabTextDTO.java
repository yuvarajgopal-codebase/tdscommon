package com.tdsecurities.common.dto;

import java.util.List;

public class TabTextDTO {

    private String title;
    private String imageId;
    private String description;

    private String tabHeader;
    private List<TabTextItemDTO> tabs;
    

    @Override
    public String toString() {
        return "TabTextDTO [ description=" + description + ", imageId=" + imageId + ", title=" + title + "]";
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the imageId
     */
    public String getImageId() {
        return imageId;
    }

    /**
     * @param imageId the imageId to set
     */
    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the tabHeader
     */
    public String getTabHeader() {
        return tabHeader;
    }

    /**
     * @param tabHeader the tabHeader to set
     */
    public void setTabHeader(String tabHeader) {
        this.tabHeader = tabHeader;
    }

    /**
     * @return the tabs
     */
    public List<TabTextItemDTO> getTabs() {
        return tabs;
    }

    /**
     * @param tabs the tabs to set
     */
    public void setTabs(List<TabTextItemDTO> tabs) {
        this.tabs = tabs;
    }

}