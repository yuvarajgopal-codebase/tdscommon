        <!--Start Leadership Grid Section-->
        <section class="td-leadership">
            <div class="leadership-heading">
                <div class="td-container">
                    <div class="td-row">
                        <div class="td-col-xs-12 td-col-md-10 td-col-md-offset-1">
                            <h1>${title}</h1>
                            <div class="leadership-subtitle">
                                <p>${subtitle}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section class="td_rq_leadership_grid td-container">
                <div class="td-row">
                    <#list profiles as profileItem>
                        ${profileItem}
                    </#list>
                </div>
            </section>
        </section>
        <!--End Leadership Grid Section-->

