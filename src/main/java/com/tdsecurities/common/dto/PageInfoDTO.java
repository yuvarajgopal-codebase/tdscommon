package com.tdsecurities.common.dto;

import com.tdsecurities.common.data.WorkflowStatus;

public class PageInfoDTO {

    private String id;
    private long version;
    private String templateId;
    private long templateVersion;
    private String notes;
    private WorkflowStatus status;

    public PageInfoDTO() {
    }
    
    public PageInfoDTO(String id, long version, String templateId, long templateVersion, String notes,
            WorkflowStatus status) {
        this.id = id;
        this.version = version;
        this.templateId = templateId;
        this.templateVersion = templateVersion;
        this.notes = notes;
        this.status = status;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public long getTemplateVersion() {
        return templateVersion;
    }

    public void setTemplateVersion(long templateVersion) {
        this.templateVersion = templateVersion;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public WorkflowStatus getStatus() {
        return status;
    }

    public void setStatus(WorkflowStatus status) {
        this.status = status;
    }

}