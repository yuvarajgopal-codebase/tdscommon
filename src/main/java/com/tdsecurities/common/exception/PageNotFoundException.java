package com.tdsecurities.common.exception;

public class PageNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public PageNotFoundException() {
    }

    public PageNotFoundException(String message) {
        super(message);
    }

    public PageNotFoundException(Throwable cause) {
        super(cause);
    }

    public PageNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

}