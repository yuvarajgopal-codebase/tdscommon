package com.tdsecurities.common.exception;

public class PageGenerationException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public PageGenerationException() {
    }

    public PageGenerationException(String message) {
        super(message);
    }

    public PageGenerationException(Throwable cause) {
        super(cause);
    }

    public PageGenerationException(String message, Throwable cause) {
        super(message, cause);
    }

}