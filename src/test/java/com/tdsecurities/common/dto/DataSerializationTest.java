package com.tdsecurities.common.dto;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.hamcrest.CoreMatchers.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tdsecurities.common.component.ArticleType;

import org.junit.Test;

public class DataSerializationTest {

    private static class ComponentHolder {

        private Map<String, Object> components;

        public Map<String, Object> getComponents() {
            return components;
        }

        public void setComponents(Map<String, Object> components) {
            this.components = components;
        }

        
    }

    @Test
    public void generateLeadershipPageDataEN() {

        ComponentHolder componentHolder = new ComponentHolder();

        Map<String, Object> components = new HashMap<>();

        BannerDTO bannerDTO = new BannerDTO();
        bannerDTO.setTitle("Leadership banner");
        bannerDTO.setImageId("LeadershipBanner");
        bannerDTO.setDescription("Partner with seasoned experts the industry respects");
        components.put("leadershipBanner", bannerDTO);

        List<ProfileDTO> profileList = new ArrayList<>();

        ProfileDTO profile1 = new ProfileDTO();
        profile1.setImageId("BobDorrance");
        profile1.setImageAlt("Bob Dorrance image");
        profile1.setName("Bob Dorrance");
        profile1.setTitle("CEO");
        profile1.setDescription("Chairmain and CEO");
        profile1.setProfilePageId("ProfileBobDorrance");
        profileList.add(profile1);

        ProfileDTO profile2 = new ProfileDTO();
        profile2.setImageId("BobDorrance");
        profile2.setImageAlt("Bob Dorrance image");
        profile2.setName("Bob Dorrance");
        profile2.setTitle("CEO");
        profile2.setDescription("Chairmain and CEO");
        profile2.setProfilePageId("ProfileBobDorrance");
        profileList.add(profile2);

        LeadershipGridDTO leadershipGrid = new LeadershipGridDTO();
        leadershipGrid.setTitle("Our Executive Team");
        leadershipGrid.setSubtitle("At TD Securities, our competitive advantage lies "
            + "with our people, including the strength of our seniour executive group.");
        leadershipGrid.setProfiles(profileList);
        components.put("leadershipGrid", leadershipGrid);

        List<ArticleDTO> articles = new ArrayList<>();

        ArticleDTO article1 = new ArticleDTO();
        article1.setImageId("ArticleImage1");
        article1.setImageAlt("Article image");
        article1.setArticleType(ArticleType.LinkedArticle);
        article1.setArticlePageId("Article1Page");
        article1.setTitle("How is your Ontario auto insurance premium determined");
        article1.setDescription("Here's where you get the inside track on basic as well as optional coverages.");
        article1.setDateLabel("Published: ");
        article1.setDate("20/06/2017");
        articles.add(article1);

        ArticleListDTO articleList = new ArticleListDTO();
        articleList.setTitle("Discover new insight on markets everyday");
        articleList.setArticles(articles);
        components.put("articleList", articleList);

         componentHolder.setComponents(components);

        try {
            // Serialize the data
            Gson gson = new Gson();
            String serializedData = gson.toJson(componentHolder);
            System.out.println(serializedData);

            // Deserialize the data and check
            // ComponentHolder deserializedData = gson.fromJson(serializedData, ComponentHolder.class);
            // Map<String, Object> deserializedComponents = deserializedData.getComponents();

            JsonParser parser = new JsonParser();
            JsonElement rootElement = parser.parse(serializedData);
            JsonObject rootObject = rootElement.getAsJsonObject();

            JsonElement componentListElement = rootObject.get("components");
            JsonObject componentListObject = componentListElement.getAsJsonObject();

            JsonElement bannerElement = componentListObject.get("leadershipBanner");
            BannerDTO deserializedBannerDTO = new Gson().fromJson(bannerElement, BannerDTO.class);
            assertThat(bannerDTO.getTitle(), is(deserializedBannerDTO.getTitle()));

        } catch (Exception e) {
            System.out.println("Error in test: " + e.toString());
            fail("Test failed");
        }

    }
}
