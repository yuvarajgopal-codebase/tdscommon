package com.tdsecurities.common.dto;

import com.tdsecurities.common.component.LinkAction;
import com.tdsecurities.common.component.LinkType;

public class LinkDTO {

    private LinkType linkType;
    private LinkAction linkAction;
    private String pageId;
    private String externalUrl;
    private String description;

    public LinkType getLinkType() {
        return linkType;
    }

    public void setLinkType(LinkType linkType) {
        this.linkType = linkType;
    }

    public LinkAction getLinkAction() {
        return linkAction;
    }

    public void setLinkAction(LinkAction linkAction) {
        this.linkAction = linkAction;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getExternalUrl() {
        return externalUrl;
    }

    public void setExternalUrl(String externalUrl) {
        this.externalUrl = externalUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "LinkDTO [description=" + description + ", externalUrl=" + externalUrl + ", linkAction=" + linkAction
                + ", linkType=" + linkType + ", pageId=" + pageId + "]";
    }
    
}