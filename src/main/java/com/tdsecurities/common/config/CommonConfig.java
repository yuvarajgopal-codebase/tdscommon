package com.tdsecurities.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

@Configuration
@PropertySource("classpath:common-config.properties")
public class CommonConfig {

    @Autowired
    private Environment env;

    @Bean
    public CommonConstants commonConstants() {
        CommonConstants commonConstants = new CommonConstants();
        commonConstants.setPageContext(env.getProperty("page.context"));
        commonConstants.setImageContext(env.getProperty("image.context"));
        commonConstants.setDefaultLanguage("en_CA");
        return commonConstants;
    }

    /*
    @Bean
    public freemarker.template.Configuration freeMarkerConfiguration() {
        freemarker.template.Configuration configuration = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_28);
        configuration.setClassForTemplateLoading(CommonConfig.class, "/templates");
        configuration.setDefaultEncoding("UTF-8");
        return configuration;
    }
    */

    @Bean
    public FreeMarkerConfigurer freeMarkerConfigurer() {
        FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
        freeMarkerConfigurer.setTemplateLoaderPath("classpath:/templates");
        freeMarkerConfigurer.setDefaultEncoding("UTF-8");
        freeMarkerConfigurer.setPreferFileSystemAccess(false);
        return freeMarkerConfigurer;
    }
}