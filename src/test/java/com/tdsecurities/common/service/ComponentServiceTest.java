package com.tdsecurities.common.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.when;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.tdsecurities.common.component.ComponentType;
import com.tdsecurities.common.component.LinkAction;
import com.tdsecurities.common.config.CommonConstants;
import com.tdsecurities.common.dto.BannerDTO;
import com.tdsecurities.common.dto.LinkDTO;
import com.tdsecurities.common.testutil.FreeMarkerTestUtil;

import org.junit.Test;
import org.mockito.Mockito;

public class ComponentServiceTest {

    @Test 
    public void testGenerateContentForComponent_NullComponentType() throws Exception {
        System.out.println("Running testGenerateContentForComponent_NullComponentType()");
        JsonPrimitive jsonPrimitive = new JsonPrimitive("SimpleString");
        ComponentService componentService = new ComponentService();
        String result = componentService.generateContentForComponent(null, jsonPrimitive);
        assertNull(result);
    }

    @Test
    public void testGenerateContentForComponent_SimpleTextComponentType() throws Exception {
        System.out.println("Running testGenerateContentForComponent_SimpleTextComponentType()");
        JsonPrimitive jsonPrimitive = new JsonPrimitive("SimpleString");
        ComponentService componentService = new ComponentService();
        String result = componentService.generateContentForComponent(ComponentType.SimpleText, jsonPrimitive);
        assertThat(result, is("SimpleString"));

    }

    @Test
    public void testGenerateContentForComponent_BannerComponentType() throws Exception {
        System.out.println("Running testGenerateContentForComponent_BannerComponentType()");
        CommonConstants constantsMock = Mockito.mock(CommonConstants.class);
        when(constantsMock.getPageContext()).thenReturn("/pageContext");
        when(constantsMock.getImageContext()).thenReturn("/imageContext");

        BannerDTO bannerDTO = new BannerDTO();
        bannerDTO.setTitle("Banner Title");
        bannerDTO.setImageId("Image ID");
        bannerDTO.setDescription("Banner Description");
        LinkDTO linkDTO = new LinkDTO();
        linkDTO.setDescription("Link Description");
        linkDTO.setPageId("Page ID");
        linkDTO.setLinkAction(LinkAction.OpenInline);
        bannerDTO.setCtaButton(linkDTO);
    
        String jsonString = new Gson().toJson(bannerDTO);
        System.out.println("JSON banner: " + jsonString);

        JsonParser parser = new JsonParser();
        JsonElement jsonElement = parser.parse(jsonString);

        ComponentService componentService = new ComponentService();
        componentService.commonConstants = constantsMock;
        componentService.freeMarkerConfiguration = FreeMarkerTestUtil.getTestFreeMarkerConfiguration();
        String result = componentService.generateContentForComponent(ComponentType.Banner, jsonElement);
        
        assertNotNull(result);
        assertThat(result, containsString("Banner Title"));
        assertThat(result, containsString("Image ID"));
        assertThat(result, containsString("Page ID"));

    }


    @Test
    public void testGetComponentFromJson_BannerComponentType() throws Exception {
        System.out.println("Running testGetComponentFromJson_BannerComponentType()");
        CommonConstants constantsMock = Mockito.mock(CommonConstants.class);
        when(constantsMock.getPageContext()).thenReturn("/pageContext");
        when(constantsMock.getImageContext()).thenReturn("/imageContext");

        BannerDTO bannerDTO = new BannerDTO();
        bannerDTO.setTitle("Banner Title");
        bannerDTO.setImageId("Image ID");
        bannerDTO.setDescription("Banner Description");
        LinkDTO linkDTO = new LinkDTO();
        linkDTO.setDescription("Link Description");
        linkDTO.setPageId("Page ID");
        linkDTO.setLinkAction(LinkAction.OpenInline);
        bannerDTO.setCtaButton(linkDTO);
    
        String jsonString = new Gson().toJson(bannerDTO);
        System.out.println("JSON banner: " + jsonString);

        JsonParser parser = new JsonParser();
        JsonElement jsonElement = parser.parse(jsonString);

        ComponentService componentService = new ComponentService();
        componentService.commonConstants = constantsMock;
        componentService.freeMarkerConfiguration = FreeMarkerTestUtil.getTestFreeMarkerConfiguration();
        Object result = componentService.getComponentFromJson(ComponentType.Banner, jsonElement);
        
        assertNotNull(result);
        assertThat(result, instanceOf(BannerDTO.class));
        BannerDTO deserializedBannerDTO = (BannerDTO)result;
        assertThat(deserializedBannerDTO.getTitle(), is("Banner Title"));
    }

}