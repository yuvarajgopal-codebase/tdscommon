package com.tdsecurities.common.dto;

import com.tdsecurities.common.data.ImageSize;

public class ImageDTO {

    private String id;
    private long version;
    private String mimeType;
    private ImageSize size;
    private byte[] imageData;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public ImageSize getSize() {
        return size;
    }

    public void setSize(ImageSize size) {
        this.size = size;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }
    
    @Override
    public String toString() {
        return "ImageDTO [id=" + id + ", mimeType=" + mimeType + ", size=" + size + ", version=" + version + "]";
    }

}