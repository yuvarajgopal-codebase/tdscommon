package com.tdsecurities.common.dto;

import com.tdsecurities.common.data.WorkflowStatus;

public class WorkflowEventDTO {

    private String message;
    private WorkflowStatus fromStatus;
    private WorkflowStatus toStatus;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public WorkflowStatus getFromStatus() {
        return fromStatus;
    }

    public void setFromStatus(WorkflowStatus fromStatus) {
        this.fromStatus = fromStatus;
    }

    public WorkflowStatus getToStatus() {
        return toStatus;
    }

    public void setToStatus(WorkflowStatus toStatus) {
        this.toStatus = toStatus;
    }

}