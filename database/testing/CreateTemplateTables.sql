/* Template Tables */
CREATE TABLE tdspw_template_info (
	
	id VARCHAR(50) NOT NULL,
	version INT NOT NULL,
	template_file VARCHAR(512) NULL,
	active BIT NOT NULL,
	created_at DATETIME NOT NULL,
	created_by VARCHAR(255) NOT NULL,
	updated_at DATETIME NULL,
	updated_by VARCHAR(255) NULL,
	CONSTRAINT PK_template_info PRIMARY KEY (id, version)
);

CREATE TABLE tdspw_template_component (
	id INT IDENTITY(1,1) PRIMARY KEY,
	template_id VARCHAR(50) NOT NULL,
	template_version INT NOT NULL,
	sort_order INT NOT NULL,
	name VARCHAR(100) NOT NULL,
	type VARCHAR(100) NOT NULL,
	description VARCHAR(512) NULL,
	created_at DATETIME NOT NULL,
	created_by VARCHAR(255) NOT NULL,
	updated_at DATETIME NULL,
	updated_by VARCHAR(255) NULL,
	FOREIGN KEY (template_id, template_version) REFERENCES tdspw_template_info(id, version)
);

CREATE INDEX template_idx 
ON tdspw_template_component (template_id, template_version);

CREATE INDEX order_idx 
ON tdspw_template_component (sort_order);

CREATE TABLE tdspw_template_image (
	id INT IDENTITY(1,1) PRIMARY KEY,
	template_id VARCHAR(50) NOT NULL,
	template_version INT NOT NULL,
	image_data IMAGE NULL,
	created_at DATETIME NOT NULL,
	created_by VARCHAR(255) NOT NULL,
	updated_at DATETIME NULL,
	updated_by VARCHAR(255) NULL,
	FOREIGN KEY (template_id, template_version) REFERENCES tdspw_template_info(id, version)	
);

CREATE INDEX template_idx 
ON tdspw_template_image (template_id, template_version);


---- Cleanup ----

DROP INDEX order_idx ON tdspw_template_component;

DROP INDEX template_idx ON tdspw_template_component;

DROP INDEX template_idx ON tdspw_template_image;

DROP TABLE tdspw_template_component;

DROP TABLE tdspw_template_image;

DROP TABLE tdspw_template_info;