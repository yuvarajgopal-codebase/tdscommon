package com.tdsecurities.common.component;

public enum LinkAction {
    OpenInline,
    OpenInNewWindow
}