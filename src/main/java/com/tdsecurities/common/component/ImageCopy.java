package com.tdsecurities.common.component;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import com.tdsecurities.common.config.CommonConstants;
import com.tdsecurities.common.dto.ImageCopyDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class ImageCopy implements BaseComponent {

    private static final Logger logger = LoggerFactory.getLogger(Banner.class);

    private ImageCopyDTO imageCopyDTO;
    private CommonConstants commonConstants;

    public ImageCopy(ImageCopyDTO imageCopyDTO, CommonConstants commonConstants) {
        this.imageCopyDTO = imageCopyDTO;
        this.commonConstants = commonConstants;
    }

    @Override
    public String generateContent(Configuration freeMarkerConfiguration) {

        try {

            Map<String, Object> templateParams = new HashMap<>();

            Template template = freeMarkerConfiguration.getTemplate("component/image_copy.ftl");
            templateParams.put("imageCopy", imageCopyDTO);
            templateParams.put("constants", commonConstants);

            StringWriter stringWriter = new StringWriter();
            template.process(templateParams, stringWriter);

            return stringWriter.toString();

        } catch (Exception e) {
            logger.error("Error generating image copy content: ", e);
            throw new RuntimeException("Error generating image copy content", e);
        }
        
	}

}