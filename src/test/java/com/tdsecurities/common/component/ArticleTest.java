package com.tdsecurities.common.component;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.when;

import com.tdsecurities.common.config.CommonConstants;
import com.tdsecurities.common.dto.ArticleDTO;
import com.tdsecurities.common.testutil.FreeMarkerTestUtil;

import org.junit.Test;
import org.mockito.Mockito;

public class ArticleTest {

    @Test
    public void testGenerateContent() throws Exception {

        CommonConstants constantsMock = Mockito.mock(CommonConstants.class);
        when(constantsMock.getPageContext()).thenReturn("/pageContext");
        when(constantsMock.getImageContext()).thenReturn("/imageContext");

        ArticleDTO articleDTO = new ArticleDTO();
        articleDTO.setImageId("ImageID");
        articleDTO.setImageAlt("ImageAlt");
        articleDTO.setArticleType(ArticleType.LinkedArticle);
        articleDTO.setArticlePageId("TestPage");
        articleDTO.setTitle("ArticleTitle");
        articleDTO.setDateLabel("Published:");
        articleDTO.setDate("20/06/2017");
        articleDTO.setDescription("This is the description for the article");

        Article article = new Article(articleDTO, constantsMock);
        String articleHtml = article.generateContent(FreeMarkerTestUtil.getTestFreeMarkerConfiguration());

        System.out.println("Article HTML\n" + articleHtml);

        assertThat(articleHtml, containsString("ImageID"));
        assertThat(articleHtml, containsString("ImageAlt"));
        assertThat(articleHtml, containsString("TestPage"));
        assertThat(articleHtml, containsString("ArticleTitle"));
        assertThat(articleHtml, containsString("Published:"));
        assertThat(articleHtml, containsString("20/06/2017"));
        assertThat(articleHtml, containsString("This is the description for the article"));
    
    }
}