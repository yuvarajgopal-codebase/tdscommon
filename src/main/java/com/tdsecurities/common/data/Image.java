package com.tdsecurities.common.data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "tdspw_image")
@IdClass(ImageId.class)
public class Image {

    @Id
    private String id;

    @Id
    private long version;

    @Column(name = "mime_type")
    private String mimeType;

    @Column(name = "default_size")
    @Enumerated(EnumType.STRING)
    private ImageSize defaultSize;

    private String notes;

    @Enumerated(EnumType.STRING)
    private WorkflowStatus status;

    @Lob
    @Column(name = "image_xs", columnDefinition = "Image")
    private byte[] imageXs;

    @Column(name = "image_xs_filename")
    private String imageXsFilename;

    @Column(name = "image_xs_exists")
    private boolean imageXsExists;

    @Lob
    @Column(name = "image_sm", columnDefinition = "Image")
    private byte[] imageSm;

    @Column(name = "image_sm_filename")
    private String imageSmFilename;

    @Column(name = "image_sm_exists")
    private boolean imageSmExists;

    @Lob
    @Column(name = "image_md", columnDefinition = "Image")
    private byte[] imageMd;

    @Column(name = "image_md_filename")
    private String imageMdFilename;

    @Column(name = "image_md_exists")
    private boolean imageMdExists;

    @Lob
    @Column(name = "image_lg", columnDefinition = "Image")
    private byte[] imageLg;

    @Column(name = "image_lg_filename")
    private String imageLgFilename;

    @Column(name = "image_lg_exists")
    private boolean imageLgExists;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_at")
    private Date updatedAt;

    @Column(name = "updated_by")
    private String updatedBy;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public ImageSize getDefaultSize() {
        return defaultSize;
    }

    public void setDefaultSize(ImageSize defaultSize) {
        this.defaultSize = defaultSize;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public WorkflowStatus getStatus() {
        return status;
    }

    public void setStatus(WorkflowStatus status) {
        this.status = status;
    }

    public byte[] getImageXs() {
        return imageXs;
    }

    public void setImageXs(byte[] imageXs) {
        this.imageXs = imageXs;
    }

    public String getImageXsFilename() {
        return imageXsFilename;
    }

    public void setImageXsFilename(String imageXsFilename) {
        this.imageXsFilename = imageXsFilename;
    }

    public boolean isImageXsExists() {
        return imageXsExists;
    }

    public void setImageXsExists(boolean imageXsExists) {
        this.imageXsExists = imageXsExists;
    }

    public byte[] getImageSm() {
        return imageSm;
    }

    public void setImageSm(byte[] imageSm) {
        this.imageSm = imageSm;
    }

    public String getImageSmFilename() {
        return imageSmFilename;
    }

    public void setImageSmFilename(String imageSmFilename) {
        this.imageSmFilename = imageSmFilename;
    }

    public boolean isImageSmExists() {
        return imageSmExists;
    }

    public void setImageSmExists(boolean imageSmExists) {
        this.imageSmExists = imageSmExists;
    }

    public byte[] getImageMd() {
        return imageMd;
    }

    public void setImageMd(byte[] imageMd) {
        this.imageMd = imageMd;
    }

    public String getImageMdFilename() {
        return imageMdFilename;
    }

    public void setImageMdFilename(String imageMdFilename) {
        this.imageMdFilename = imageMdFilename;
    }

    public boolean isImageMdExists() {
        return imageMdExists;
    }

    public void setImageMdExists(boolean imageMdExists) {
        this.imageMdExists = imageMdExists;
    }

    public byte[] getImageLg() {
        return imageLg;
    }

    public void setImageLg(byte[] imageLg) {
        this.imageLg = imageLg;
    }

    public String getImageLgFilename() {
        return imageLgFilename;
    }

    public void setImageLgFilename(String imageLgFilename) {
        this.imageLgFilename = imageLgFilename;
    }
 
    public boolean isImageLgExists() {
        return imageLgExists;
    }

    public void setImageLgExists(boolean imageLgExists) {
        this.imageLgExists = imageLgExists;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    
}