package com.tdsecurities.common.component;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import com.tdsecurities.common.config.CommonConstants;
import com.tdsecurities.common.dto.ProfileDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class Profile implements BaseComponent {

    private static final Logger logger = LoggerFactory.getLogger(Profile.class);

    private ProfileDTO profileDTO;
    private CommonConstants commonConstants;

    public Profile(ProfileDTO profileDTO, CommonConstants commonConstants) {
        this.profileDTO = profileDTO;
        this.commonConstants = commonConstants;
    }

    @Override
    public String generateContent(Configuration freeMarkerConfiguration) {

        try {

            Map<String, Object> templateParams = new HashMap<>();

            Template template = freeMarkerConfiguration.getTemplate("component/profile.ftl");
            templateParams.put("profile", profileDTO);
            templateParams.put("constants", commonConstants);

            StringWriter stringWriter = new StringWriter();
            template.process(templateParams, stringWriter);

            return stringWriter.toString();

        } catch (Exception e) {
            logger.error("Error generating profile content: ", e);
            throw new RuntimeException("Error generating profile content", e);
        }
        
	}

}