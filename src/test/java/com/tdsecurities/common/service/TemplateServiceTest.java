package com.tdsecurities.common.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.*;


import java.util.ArrayList;
import java.util.List;

import com.tdsecurities.common.data.TemplateInfo;
import com.tdsecurities.common.data.TemplateInfoRepository;
import com.tdsecurities.common.dto.TemplateInfoDTO;

import org.junit.Test;
import org.mockito.Mockito;

public class TemplateServiceTest {

    @Test
    public void testGetTemplates_Success() throws Exception {
        System.out.println("Running testGetTemplates_Success()");

        TemplateInfoRepository templateInfoRepositoryMock = Mockito.mock(TemplateInfoRepository.class);

        List<TemplateInfo> templateInfoMockResults = new ArrayList<TemplateInfo>();
        templateInfoMockResults.add(new TemplateInfo());
        templateInfoMockResults.add(new TemplateInfo());
        when(templateInfoRepositoryMock.findAll()).thenReturn(templateInfoMockResults);

        TemplateService templateService = new TemplateService();
        templateService.templateInfoRepository = templateInfoRepositoryMock;
        List<TemplateInfoDTO> templateInfoList = templateService.getTemplates();
        verify(templateInfoRepositoryMock, times(1)).findAll();
        assertNotNull(templateInfoList);
        assertThat(templateInfoList.size(), is(2));

    }
}


