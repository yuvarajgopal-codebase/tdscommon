package com.tdsecurities.common.dto;

import java.util.Map;

public class PageUpdateDTO {

    private PageInfoDTO pageInfo;
    private String title;
    private String analyticsTag;
    private String seoMetadata;
    private Map<String, Object> pageData;

    public PageInfoDTO getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfoDTO pageInfo) {
        this.pageInfo = pageInfo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnalyticsTag() {
        return analyticsTag;
    }

    public void setAnalyticsTag(String analyticsTag) {
        this.analyticsTag = analyticsTag;
    }

    public String getSeoMetadata() {
        return seoMetadata;
    }

    public void setSeoMetadata(String seoMetadata) {
        this.seoMetadata = seoMetadata;
    }

    public Map<String, Object> getPageData() {
        return pageData;
    }

    public void setPageData(Map<String, Object> pageData) {
        this.pageData = pageData;
    }

    @Override
    public String toString() {
        return "PageUpdateDTO [analyticsTag=" + analyticsTag + ", pageData=" + pageData + ", pageInfo=" + pageInfo
                + ", seoMetadata=" + seoMetadata + ", title=" + title + "]";
    }

}