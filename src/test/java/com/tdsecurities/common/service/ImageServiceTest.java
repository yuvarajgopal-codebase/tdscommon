package com.tdsecurities.common.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.any;
import static org.hamcrest.CoreMatchers.*;

import com.tdsecurities.common.auth.AuthenticationFacade;
import com.tdsecurities.common.data.ActiveImage;
import com.tdsecurities.common.data.ActiveImageRepository;
import com.tdsecurities.common.data.Image;
import com.tdsecurities.common.data.ImageId;
import com.tdsecurities.common.data.ImageMetadata;
import com.tdsecurities.common.data.ImageRepository;
import com.tdsecurities.common.data.ImageSize;
import com.tdsecurities.common.data.WorkflowStatus;
import com.tdsecurities.common.dto.ImageMetadataDTO;
import com.tdsecurities.common.exception.ItemNotFoundException;

import org.junit.Test;
import org.mockito.Mockito;

public class ImageServiceTest {

    @Test
    public void testSaveSingleImage_Success() throws Exception {
        System.out.println("Running testSaveImage_Success()");

        ImageRepository imageRepositoryMock = Mockito.mock(ImageRepository.class);
        when(imageRepositoryMock.existsById(any(ImageId.class))).thenReturn(false);

        Image mockImage = new Image();
        mockImage.setId("TestImage");
        mockImage.setVersion(1);
        mockImage.setMimeType("image/png");
        mockImage.setDefaultSize(ImageSize.md);
        mockImage.setImageMdExists(true);
        mockImage.setNotes("This is a test");
        mockImage.setStatus(WorkflowStatus.Draft);
        mockImage.setImageMd("TestMdImage".getBytes());
        mockImage.setImageMdFilename("SingleImage.jpg");
        when(imageRepositoryMock.save(any())).thenReturn(mockImage); 

        ActiveImageRepository activeImageRepositoryMock = Mockito.mock(ActiveImageRepository.class);
        when(activeImageRepositoryMock.save(any())).thenReturn(new ActiveImage());

        AuthenticationFacade authenticationFacadeMock = Mockito.mock(AuthenticationFacade.class);
        when(authenticationFacadeMock.getAuthenticatedUserName()).thenReturn("MockUser");
        
        ImageService imageService = new ImageService();
        imageService.imageRepository = imageRepositoryMock;
        imageService.activeImageRepository = activeImageRepositoryMock;
        imageService.authenticationFacade = authenticationFacadeMock;

        ImageMetadataDTO createdImage = imageService.saveSingleImage("TestImage", "image/png", "This is a test",
            "TestSingleImage".getBytes(), "SingleImage.jpg");
        verify(imageRepositoryMock, times(1)).existsById(any());
        verify(imageRepositoryMock, times(1)).save(any());
        verify(activeImageRepositoryMock, times(1)).save(any());
        verify(authenticationFacadeMock, times(2)).getAuthenticatedUserName();
        assertNotNull(createdImage);
        assertThat(createdImage.getId(), is("TestImage"));
        assertThat(createdImage.isImageXsExists(), is(false));
        assertThat(createdImage.isImageSmExists(), is(false));
        assertThat(createdImage.isImageMdExists(), is(true));
        assertThat(createdImage.isImageLgExists(), is(false));
    }    

    @Test
    public void testGetImageMetadataForAllImages_Success() throws Exception {

        List<ImageMetadata> imageMetadataList = new ArrayList<>();
        ImageMetadata mockImageMetadata = Mockito.mock(ImageMetadata.class);
        imageMetadataList.add(mockImageMetadata);

        ImageRepository imageRepositoryMock = Mockito.mock(ImageRepository.class);
        when(imageRepositoryMock.findAllImageMetadata()).thenReturn(imageMetadataList);

        ImageService imageService = new ImageService();
        imageService.imageRepository = imageRepositoryMock;

        List<ImageMetadataDTO> imageMetadataDtoList = imageService.getImageMetadataForAllImages();
        verify(imageRepositoryMock, times(1)).findAllImageMetadata();
        assertNotNull(imageMetadataDtoList);
        assertThat(imageMetadataDtoList.size(), is(1));


    }

    @Test(expected = ItemNotFoundException.class)
    public void testGetImageMetadataForAllImages_NotFound() throws Exception {

        List<ImageMetadata> imageMetadataList = new ArrayList<>();
        ImageRepository imageRepositoryMock = Mockito.mock(ImageRepository.class);
        when(imageRepositoryMock.findAllImageMetadata()).thenReturn(imageMetadataList);

        ImageService imageService = new ImageService();
        imageService.imageRepository = imageRepositoryMock;

        List<ImageMetadataDTO> imageMetadataDtoList = imageService.getImageMetadataForAllImages();
    }

}