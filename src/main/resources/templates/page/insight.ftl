    <#if insightBanner?has_content>
    ${insightBanner}
    </#if>

    <#if articleList?has_content>
        <div class="alternate-bg-color-sections">
        ${articleList}
        </div>
    </#if>