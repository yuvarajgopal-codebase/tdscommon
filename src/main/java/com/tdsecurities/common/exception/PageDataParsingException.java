package com.tdsecurities.common.exception;

public class PageDataParsingException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public PageDataParsingException() {
    }

    public PageDataParsingException(String message) {
        super(message);
    }

    public PageDataParsingException(Throwable cause) {
        super(cause);
    }

    public PageDataParsingException(String message, Throwable cause) {
        super(message, cause);
    }

}