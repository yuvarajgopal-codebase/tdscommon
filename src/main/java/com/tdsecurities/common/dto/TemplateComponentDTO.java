package com.tdsecurities.common.dto;

import com.tdsecurities.common.component.ComponentType;

public class TemplateComponentDTO {

    private String name;
    private Long sortOrder;
    private ComponentType componentType;
    private String description;
    
    public TemplateComponentDTO() {
    }

    public TemplateComponentDTO(String name, Long sortOrder, ComponentType componentType, String description) {
        this.name = name;
        this.sortOrder = sortOrder;
        this.componentType = componentType;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Long sortOrder) {
        this.sortOrder = sortOrder;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "TemplateComponentDTO [componentType=" + componentType + ", description=" + description + ", name="
                + name + ", sortOrder=" + sortOrder + "]";
    }

    
}