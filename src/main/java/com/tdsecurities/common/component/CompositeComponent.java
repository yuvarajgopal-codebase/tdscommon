package com.tdsecurities.common.component;

import java.util.ArrayList;
import java.util.List;

import freemarker.template.Configuration;

public abstract class CompositeComponent<T extends BaseComponent> {

    private List<T> componentList = new ArrayList<>();

    public List<String> getGeneratedComponentContent(Configuration freeMarkerConfiguration) {


        List<String> generatedComponentList = new ArrayList<>();

        for (T component: componentList) {
            generatedComponentList.add(component.generateContent(freeMarkerConfiguration));
        }

        return generatedComponentList;
    }

    void add(T subcomponent) {
        componentList.add(subcomponent);
    }

    List<T> getSubcomponents() {
        return componentList;
    }

}