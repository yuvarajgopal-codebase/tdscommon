package com.tdsecurities.common.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TemplateComponentRepository extends CrudRepository<TemplateComponent, Long> {

    List<TemplateComponent> findByTemplateIdAndTemplateVersion(String templateId, long templateVersion);
    List<TemplateComponent> findByTemplateIdAndTemplateVersionOrderBySortOrder(String templateId, long templateVersion);
    
}