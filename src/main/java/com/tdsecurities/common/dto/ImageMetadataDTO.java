package com.tdsecurities.common.dto;

import com.tdsecurities.common.data.ImageSize;
import com.tdsecurities.common.data.WorkflowStatus;

public class ImageMetadataDTO {

    private String id;
    private long version;
    private String mimeType;
    private ImageSize defaultSize;
    private String notes;
    private WorkflowStatus status;
    private boolean imageXsExists;
    private boolean imageSmExists;
    private boolean imageMdExists;
    private boolean imageLgExists;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public ImageSize getDefaultSize() {
        return defaultSize;
    }

    public void setDefaultSize(ImageSize defaultSize) {
        this.defaultSize = defaultSize;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public WorkflowStatus getStatus() {
        return status;
    }

    public void setStatus(WorkflowStatus status) {
        this.status = status;
    }

    public boolean isImageXsExists() {
        return imageXsExists;
    }

    public void setImageXsExists(boolean imageXsExists) {
        this.imageXsExists = imageXsExists;
    }

    public boolean isImageSmExists() {
        return imageSmExists;
    }

    public void setImageSmExists(boolean imageSmExists) {
        this.imageSmExists = imageSmExists;
    }

    public boolean isImageMdExists() {
        return imageMdExists;
    }

    public void setImageMdExists(boolean imageMdExists) {
        this.imageMdExists = imageMdExists;
    }

    public boolean isImageLgExists() {
        return imageLgExists;
    }

    public void setImageLgExists(boolean imageLgExists) {
        this.imageLgExists = imageLgExists;
    }

    @Override
    public String toString() {
        return "ImageMetadataDTO [defaultSize=" + defaultSize + ", id=" + id + ", imageLgExists=" + imageLgExists
                + ", imageMdExists=" + imageMdExists + ", imageSmExists=" + imageSmExists + ", imageXsExists="
                + imageXsExists + ", mimeType=" + mimeType + ", notes=" + notes + ", status=" + status + ", version="
                + version + "]";
    }

}