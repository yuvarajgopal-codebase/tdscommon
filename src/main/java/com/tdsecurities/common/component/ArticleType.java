package com.tdsecurities.common.component;

public enum ArticleType {
    LinkedArticle,
    VideoArticle
}