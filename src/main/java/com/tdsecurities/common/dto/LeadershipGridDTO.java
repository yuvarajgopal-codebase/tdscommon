package com.tdsecurities.common.dto;

import java.util.List;

public class LeadershipGridDTO {

    private String title;
    private String subtitle;
    private List<ProfileDTO> profiles;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public List<ProfileDTO> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<ProfileDTO> profiles) {
        this.profiles = profiles;
    }

    @Override
    public String toString() {
        return "LeadershipGridDTO [profiles=" + profiles + ", subtitle=" + subtitle + ", title=" + title + "]";
    }
    
}