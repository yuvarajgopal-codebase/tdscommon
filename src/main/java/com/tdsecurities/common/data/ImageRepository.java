package com.tdsecurities.common.data;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends CrudRepository<Image, ImageId> {

    // Create 
    // @Query("select i.id as id, i.version as version, i.mime_type as mimeType, i.default_size as defaultSize, i.notes as notes, i.status as status, i.image_xs_exists as imageXsExists, i.image_sm_exists as imageSmExists, i.image_md_exists as imageMdExists, i.image_lg_exists as imageLgExists from Image ii")
    @Query("select i.id as id, i.version as version, i.mimeType as mimeType, i.defaultSize as defaultSize, i.notes as notes, i.status as status, i.imageXsExists as imageXsExists, i.imageSmExists as imageSmExists, i.imageMdExists as imageMdExists, i.imageLgExists as imageLgExists from Image i")
    List<ImageMetadata> findAllImageMetadata();

}
