package com.tdsecurities.common.component;

public enum ComponentType {
    SimpleText,
    Image,
    Profile,
    LeadershipGrid,
    Banner,
    TextWithButtons,
    ArticleList,
    ImageCopy,
    TabText
    
}