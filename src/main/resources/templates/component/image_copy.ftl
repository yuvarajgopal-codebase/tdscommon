<!--Start Image and Copy Section-->

<section class="td_rq_table-image-right td-image-block-right-with-copy">
            <div class="td-col td-col-sm-12">
                <div class="vis">
                    <!-- <div class="td-col td-col-xs-12 td-col-sm-6 td-col-sm-offset-6 td-col-md-offset-6 td-col-md-6 td-col-lg-6 td-col-lg-offset-6 td-extend-left td-extend-right"> -->
                    <div class="td-col td-col-xs-12 td-col-sm-6 td-col-sm-offset-0 td-col-md-offset-0 td-col-md-6 td-col-lg-6 td-col-lg-offset-0 td-extend-left td-extend-right">
                        <picture class="td-underwriting-image">
                            <source srcset="${constants.imageContext}/${imageCopy.imageId}?size=lg" media="(min-width: 1200px)">
                            <source srcset="${constants.imageContext}/${imageCopy.imageId}?size=md" media="(min-width: 1024px)">
                            <!-- Following is a fallback for IE. It makes sense to serve the lg image because we know it's a desktop -->
                            <img srcset="${constants.imageContext}/${imageCopy.imageId}?size=sm" alt="" media="(max-width: 1024px)">
                        </picture>
                    </div>
                </div>

                <div class="td-container-fluid">
                    <div class="td-row">
                        <div>
                            <!-- copy col -->
                            <!-- <div class="td-col td-col-xs-12 td-col-md-6 td-col-md-offset-0 td-col-sm-6 td-col-sm-offset-0 td-col-xs-6 td-col-lg-5 td-col-lg-offset-1 td-copy-container"> -->
                            <div class="td-col td-col-xs-12 td-col-md-6 td-col-md-offset-6 td-col-lg-6 td-col-lg-offset-6 td-copy-container">
                                <div>
                                    <div>
                                        <div>
                                            <h2> ${imageCopy.title} </h2>

                                            <div class="rte">
                                                <p class="td-rte-margin-top td-rte-margin-top-none">
                                                    ${imageCopy.description}
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </section>

<!--End Image and Copy Section-->