<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <title>${pageTitle}</title>
    <link rel="shortcut icon" href="https://www.tdcanadatrust.com/images/evergreen/ui/favicon.ico" type="image/vnd.microsoft.icon"/>
    
    <!--  CSS -->
    <link href="/assets/css/default.css" rel="stylesheet">
    <link href="/assets/css/app.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <link href="/assets/css/stylesie89.css" rel="stylesheet">
    <![endif]-->

    <!-- Alternate section color style -->
    <style>
    .alternate-bg-color-sections:nth-of-type(odd) {
        background-color: white;
    }
    .alternate-bg-color-sections:nth-of-type(even) {
        background-color: #f3f3f8!important;
    }
    </style>
</head>
<body>
    ${header}

    <!--Start Main Content area-->
	<div class="td-contentarea" role="main" id="main">
    ${pageContent}
    </div>

    ${footer}
	<script src="/assets/js/libraries.js"></script>
    <script src="/assets/js/default.js"></script>
    <script src="/assets/js/tdcustom.js"></script>
</body>
</html>