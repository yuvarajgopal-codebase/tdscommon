package com.tdsecurities.common.dto;

public class TemplateInfoDTO {

    private String id;
    private long version;
    private String templateFile;
    private boolean active;

    public TemplateInfoDTO() {
    }

    public TemplateInfoDTO(String id, long version, String templateFile, boolean active) {
        this.id = id;
        this.version = version;
        this.templateFile = templateFile;
        this.active = active;
    }
 
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public String getTemplateFile() {
        return templateFile;
    }

    public void setTemplateFile(String templateFile) {
        this.templateFile = templateFile;
    }
    
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "TemplateInfoDTO [active=" + active + ", id=" + id + ", templateFile=" + templateFile + ", version="
                + version + "]";
    }
  
}