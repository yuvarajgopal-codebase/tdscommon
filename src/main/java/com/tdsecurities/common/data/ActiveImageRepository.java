package com.tdsecurities.common.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActiveImageRepository extends CrudRepository<ActiveImage, String> {

}