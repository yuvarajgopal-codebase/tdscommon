package com.tdsecurities.common.data;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TemplateInfoRepository extends CrudRepository<TemplateInfo, TemplateInfoId> {

    List<TemplateInfo> findAll();

}