package com.tdsecurities.common.data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tdspw_active_page")
public class ActivePage {

    @Id
    @Column(name = "page_id")
    private String pageId;

    @Column(name ="live_version")
    private long liveVersion;

    @Column(name ="stage_version")
    private long stageVersion;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_at")
    private Date updatedAt;

    @Column(name = "updated_by")
    private String updatedBy;

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public long getLiveVersion() {
        return liveVersion;
    }

    public void setLiveVersion(long liveVersion) {
        this.liveVersion = liveVersion;
    }

    public long getStageVersion() {
        return stageVersion;
    }

    public void setStageVersion(long stageVersion) {
        this.stageVersion = stageVersion;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    
}