/* Main Tables - Images, Menus, Header, Footer */
CREATE TABLE tdspw_main_section (
	id VARCHAR(50) NOT NULL,
	version INT NOT NULL,
	template_id VARCHAR(50) NOT NULL,
	template_version INT NOT NULL,
	type VARCHAR(100) NOT NULL,
	notes VARCHAR(4000) NULL,
	status VARCHAR(50) NULL,
	created_at DATETIME NOT NULL,
	created_by VARCHAR(255) NOT NULL,
	updated_at DATETIME NULL,
	updated_by VARCHAR(255) NULL,
	CONSTRAINT PK_main_section PRIMARY KEY (id, version),
	FOREIGN KEY (template_id, template_version) REFERENCES tdspw_template_info(id, version)
);

CREATE TABLE tdspw_main_data (
	id INT IDENTITY(1,1) PRIMARY KEY,
	main_section_id VARCHAR(50) NOT NULL,
	main_section_version INT NOT NULL,
	language VARCHAR(25) NOT NULL,
	component_data NVARCHAR(max) NULL,
	created_at DATETIME NOT NULL,
	created_by VARCHAR(255) NOT NULL,
	updated_at DATETIME NULL,
	updated_by VARCHAR(255) NULL,
	FOREIGN KEY (main_section_id, main_section_version) REFERENCES tdspw_main_section(id, version)
);

CREATE INDEX main_data_idx 
ON tdspw_main_data (main_section_id, main_section_version, language);

CREATE TABLE tdspw_active_main_section (
	main_section_id VARCHAR(100) PRIMARY KEY,
	live_version INT NOT NULL,
	stage_version INT NOT NULL,
	created_at DATETIME NOT NULL,
	created_by VARCHAR(255) NOT NULL,
	updated_at DATETIME NULL,
	updated_by VARCHAR(255) NULL
);

CREATE TABLE tdspw_image (
	id VARCHAR(100) NOT NULL,
	version INT NOT NULL,
	mime_type VARCHAR(100) NOT NULL,
	default_size VARCHAR(50) NULL,
	notes VARCHAR(4000) NULL,
	status VARCHAR(50) NULL,
	image_xs IMAGE NULL,
	image_xs_filename VARCHAR(255),
	image_xs_exists BIT NOT NULL DEFAULT 0,
	image_sm IMAGE NULL,
	image_sm_filename VARCHAR(255),
	image_sm_exists BIT NOT NULL DEFAULT 0,
	image_md IMAGE NULL,
	image_md_filename VARCHAR(255),
	image_md_exists BIT NOT NULL DEFAULT 0,
	image_lg IMAGE NULL,
	image_lg_filename VARCHAR(255),
	image_lg_exists BIT NOT NULL DEFAULT 0,
	created_at DATETIME NOT NULL,
	created_by VARCHAR(255) NOT NULL,
	updated_at DATETIME NULL,
	updated_by VARCHAR(255) NULL,
	CONSTRAINT PK_image PRIMARY KEY (id, version)
);

CREATE TABLE tdspw_active_image (
	image_id VARCHAR(100) PRIMARY KEY,
	live_version INT NOT NULL,
	stage_version INT NOT NULL,
	created_at DATETIME NOT NULL,
	created_by VARCHAR(255) NOT NULL,
	updated_at DATETIME NULL,
	updated_by VARCHAR(255) NULL
);
