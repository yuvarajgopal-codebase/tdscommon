package com.tdsecurities.common.service;

import java.util.Date;
import java.util.List;

import com.tdsecurities.common.auth.AuthenticationFacade;
import com.tdsecurities.common.data.PageInfo;
import com.tdsecurities.common.data.PageInfoRepository;
import com.tdsecurities.common.data.WorkflowStatus;
import com.tdsecurities.common.dto.WorkflowEventDTO;
import com.tdsecurities.common.exception.PageNotFoundException;
import com.tdsecurities.common.exception.WorkflowException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WorkflowService {

    private static final Logger logger = LoggerFactory.getLogger(WorkflowService.class);

    @Autowired
    PageInfoRepository pageInfoRepository;

    @Autowired
    AuthenticationFacade authenticationFacade;


    public WorkflowEventDTO submitPageForApproval(String pageId, String submissionNotes) {

        logger.info("Processing submission for approval of page {}", pageId);

        // Get the PageInfo to update.
        PageInfo pageInfo = getDraftVersion(pageId);

        // Thrown an error if an attempt was made to update a page that was not in Draft state
        if (!pageInfo.getStatus().equals(WorkflowStatus.Draft)) {
            logger.warn("Page not updatable. There is no version of the page {} that currently supports submission for approval", pageId);
            throw new WorkflowException("Page not updatable. There is no version of the page that currently supports submission for approval");
        }

        logger.info("Found page info {} with version {} and status {} for approval submission", pageInfo.getId(), 
            pageInfo.getVersion(), pageInfo.getStatus());

        // Change the status of the page and save the changes
        pageInfo.setStatus(WorkflowStatus.Submitted);
        pageInfo.setUpdatedAt(new Date());
        pageInfo.setUpdatedBy(authenticationFacade.getAuthenticatedUserName());

        // TODO: Save a record to a new WorkflowQueue table with the submission notes
        pageInfoRepository.save(pageInfo);
      
        logger.info("Successfully submitted page for approval");
        WorkflowEventDTO workflowEventDTO = new WorkflowEventDTO();
        workflowEventDTO.setFromStatus(WorkflowStatus.Draft);
        workflowEventDTO.setToStatus(WorkflowStatus.Submitted);
        workflowEventDTO.setMessage("Successfully submitted page " + pageId + " for approval");

        return workflowEventDTO;
    }


    
    /**
     * Get the PageInfo version to submit for approval (the draft version).
     * 
     * If there is a PageInfo with a 'Draft' status, then return this version
     * Any other status is not suitable for submission for approval
     * 
     */
    private PageInfo getDraftVersion(String pageId) {

        // Check for draft version of the page
        List<PageInfo> draftPageInfoList = pageInfoRepository.findByIdAndStatusAndActive(
            pageId, WorkflowStatus.Draft, true);

        if (draftPageInfoList == null || draftPageInfoList.size() == 0) {
            logger.warn("Cannot find page {} with Draft status", pageId);
            throw new WorkflowException("Cannot find page with Draft status");
        }

        if (draftPageInfoList != null && draftPageInfoList.size() > 1) {
            logger.warn("Status mismatch. There are multiple versions of page {} with Draft status", pageId);
            throw new WorkflowException("Status mismatch. There are multiple versions of this page with Draft status");
        }

        if (draftPageInfoList != null && draftPageInfoList.size() == 1) {
            return draftPageInfoList.get(0);
        }

        throw new WorkflowException("Page cannot be submitted for approval. There page does not have Draft status");

    }
}