package com.tdsecurities.common.util;

import java.util.ArrayList;
import java.util.List;

import com.tdsecurities.common.data.Image;
import com.tdsecurities.common.data.ImageMetadata;
import com.tdsecurities.common.data.PageInfo;
import com.tdsecurities.common.data.TemplateComponent;
import com.tdsecurities.common.data.TemplateInfo;
import com.tdsecurities.common.dto.ImageMetadataDTO;
import com.tdsecurities.common.dto.PageInfoDTO;
import com.tdsecurities.common.dto.TemplateComponentDTO;
import com.tdsecurities.common.dto.TemplateInfoDTO;

public class TransformUtils {

    public static PageInfoDTO xfromPageInfo(PageInfo pageInfo) {

        if (pageInfo == null) {
            return null;
        }
        
        return new PageInfoDTO(pageInfo.getId(), pageInfo.getVersion(), pageInfo.getTemplateId(), 
            pageInfo.getTemplateVersion(), pageInfo.getNotes(), 
            pageInfo.getStatus());
    }

    public static TemplateInfoDTO xformTemplateInfo(TemplateInfo templateInfo) {

        if (templateInfo == null) {
            return null;
        }

        return new TemplateInfoDTO(templateInfo.getId(), templateInfo.getVersion(), templateInfo.getTemplateFile(),
            templateInfo.isActive());
    }

    public static List<TemplateComponentDTO> xfromTemplateComponents(List<TemplateComponent> templateComponentList) {

        if (templateComponentList == null) {
            return null;
        }

        List<TemplateComponentDTO> templateComponenDtoList = new ArrayList<TemplateComponentDTO>();

        for (TemplateComponent templateComponent: templateComponentList) {

            templateComponenDtoList.add(new TemplateComponentDTO(templateComponent.getName(), templateComponent.getSortOrder(),
                templateComponent.getComponentType(), templateComponent.getDescription()));
        }
        
        return templateComponenDtoList;
    }

    public static ImageMetadataDTO getMetadataForImage(Image image) {
        
        if (image == null) {
            return null;
        }

        ImageMetadataDTO imageMetadataDTO = new ImageMetadataDTO();
        imageMetadataDTO.setId(image.getId());
        imageMetadataDTO.setVersion(image.getVersion());
        imageMetadataDTO.setMimeType(image.getMimeType());
        imageMetadataDTO.setDefaultSize(image.getDefaultSize());
        imageMetadataDTO.setNotes(image.getNotes());
        imageMetadataDTO.setStatus(image.getStatus());
        imageMetadataDTO.setImageXsExists(image.isImageXsExists());
        imageMetadataDTO.setImageSmExists(image.isImageSmExists());
        imageMetadataDTO.setImageMdExists(image.isImageMdExists());
        imageMetadataDTO.setImageLgExists(image.isImageLgExists());
        
        return imageMetadataDTO;
    }

    public static List<ImageMetadataDTO> xformImageMetadata(List<ImageMetadata> imageMetadataList) {

        if (imageMetadataList == null) {
            return null;
        }

        List<ImageMetadataDTO> imageMetaDataDtoList = new ArrayList<>();
        
        for (ImageMetadata imageMetadata: imageMetadataList) {
            ImageMetadataDTO imageMetadataDTO = new ImageMetadataDTO();

            imageMetadataDTO.setId(imageMetadata.getId());
            imageMetadataDTO.setVersion(imageMetadata.getVersion());
            imageMetadataDTO.setMimeType(imageMetadata.getMimeType());
            imageMetadataDTO.setDefaultSize(imageMetadata.getDefaultSize());
            imageMetadataDTO.setNotes(imageMetadata.getNotes());
            imageMetadataDTO.setStatus(imageMetadata.getStatus());
            imageMetadataDTO.setImageXsExists(imageMetadata.isImageXsExists());
            imageMetadataDTO.setImageSmExists(imageMetadata.isImageSmExists());
            imageMetadataDTO.setImageMdExists(imageMetadata.isImageMdExists());
            imageMetadataDTO.setImageLgExists(imageMetadata.isImageLgExists());
    
            imageMetaDataDtoList.add(imageMetadataDTO);
        }

        return imageMetaDataDtoList;
    }
}