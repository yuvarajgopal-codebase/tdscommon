        <!--Start Banner Section-->
        <section title="${banner.title}" class="td_rq_a_banner td-a-banner a-banner-cta {parallax_top_m:46} " 
        data-bg-srcset="{&quot;lg&quot;:&quot;${constants.imageContext}/${banner.imageId}?size=lg&quot;, &quot;md&quot;:&quot;${constants.imageContext}/${banner.imageId}?size=md&quot;, &quot;sm&quot;:&quot;${constants.imageContext}/${banner.imageId}?size=sm&quot;, &quot;xs&quot;:&quot;${constants.imageContext}/${banner.imageId}?size=xs&quot;}" style="background-size: 2676px 446px; background-position: 50% 62px;">
        <!--<section title="${banner.title}" class="td_rq_a_banner td-a-banner a-banner-cta {parallax_top_m:46} " 
        data-bg-srcset="{&quot;lg&quot;:&quot;/assets/img/banner/1.1_template-desktop.png&quot;, &quot;md&quot;:&quot;/assets/img/banner/1.1_template-tl.png&quot;, &quot;sm&quot;:&quot;/assets/img/banner/1.1_template-tp.png&quot;, &quot;xs&quot;:&quot;/assets/img/banner/1.1_template-mobile.png&quot;}" id="aBanner0" style="background-size: 2676px 446px; background-position: 50% 62px;">-->

            <div class="td-container td-xs-full-bleed td-banner-content">
                <div class="td-col td-col-xs-12 td-col-md-5  td-col-md-offset-7 td-col-sm-6  td-col-sm-offset-6 td-col-lg-4 td-col-lg-offset-8">
                    <div class="td-a-banner-callout parallax-banner">
                        <div class="rte">
                            <p class="td-banner-header td-banner-header-only">${banner.description}</p>
                        </div>
                        <#if banner.ctaButton?has_content>
                        <div class="td-row callout-action">																					
                            <div class="td-col td-col-xs-10 td-col-xs-offset-1 td-col-sm-9 td-col-sm-offset-0">							
                                <button onclick="window.location.href='${constants.pageContext}/${banner.ctaButton.pageId}'" class="btn btn-block td-button-secondary" type="button">${banner.ctaButton.description}</button>			
                            </div>					
                        </div>
                        </#if>
                    </div>
                </div>
            </div>	
        </section>
        <!--End Banner Section  -->

