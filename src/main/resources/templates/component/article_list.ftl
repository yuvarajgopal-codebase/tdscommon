        <!--Start Article Section-->
        <section class="td-fullwidth">
            <section class="td_rq_simple-slidedown td-article-list">
                <div class="td-container">
                    <div class="td-row">
                        <div class="td-col-xs-12 td-col-sm-8 td-col-sm-offset-2 td-col-md-6 td-col-md-offset-3 ">
                            <div class="rte td-article-title">
                                <h2 class=" td-text-with-buttons td-rte-margin-top-none td-rte-margin-bottom-large">${title}</h2>
                            </div>
                        </div>
                        <#if articles?has_content>
                            <#if articles?size <= 3>
                                <ul class="td-list-group article">
                                <#list articles as article>
                                    ${article}
                                </#list>
                                </ul>
                            <#else>
                                <ul class="td-list-group article">
                                <#list 0..2 as i>
                                    ${articles[i]}
                                </#list>
                                </ul>

                                <!-- hidden items - it will show up when clicking the 'Load More' link -->
                                <ul class="td-list-group article hidden-article-item">
                              <#list 3..articles?size-1 as i>
                                      ${articles[i]}
                                </#list>
                                </ul>
                            </#if>
                        </#if>
                    </div>

                    <#if articles?has_content && (articles?size > 3)>
                    <div class="td-row">
                        <div class="td-col-xs-12">
                            <div class="td-load-more">
                                <a href="#" class="btn_load_more {targetelement: '.hidden-article-item', targetelementToStyle: '.td-article-list .td-load-more', removeClass: '', addClass: 'td-loaded'} td-link-nounderline td-copy-font-18">${loadMoreText}<span class="td-icon td-icon-downCaret" aria-hidden="true"></span></a>
                            </div>
                        </div>
                    </div>
                    </#if>
                </div>
            </section>
        </section>
        <!--End Articlle Section-->