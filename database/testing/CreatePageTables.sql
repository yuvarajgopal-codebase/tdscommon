/* Page Tables */

CREATE TABLE tdspw_page_info (
	id VARCHAR(50) NOT NULL,
	version INT NOT NULL,
	template_id VARCHAR(50) NOT NULL,
	template_version INT NOT NULL,
	notes VARCHAR(4000) NULL,
	status VARCHAR(50) NULL,
	active BIT NOT NULL,	
	created_at DATETIME NOT NULL,
	created_by VARCHAR(255) NOT NULL,
	updated_at DATETIME NULL,
	updated_by VARCHAR(255) NULL,
	CONSTRAINT PK_page_info PRIMARY KEY (id, version),
	FOREIGN KEY (template_id, template_version) REFERENCES tdspw_template_info(id, version)
);

CREATE INDEX status_idx 
ON tdspw_page_info(status);

CREATE TABLE tdspw_page_data (
	id INT IDENTITY(1,1) PRIMARY KEY,
	page_id VARCHAR(50) NOT NULL,
	page_version INT NOT NULL,
	language VARCHAR(25) NOT NULL,
	title VARCHAR(255) NULL,
	analytics_tag VARCHAR(255) NULL,
	seo_metadata VARCHAR(8000) NULL,
	component_data NVARCHAR(max) NULL,
	created_at DATETIME NOT NULL,
	created_by VARCHAR(255) NOT NULL,
	updated_at DATETIME NULL,
	updated_by VARCHAR(255) NULL,
	FOREIGN KEY (page_id, page_version) REFERENCES tdspw_page_info(id, version)
);

CREATE INDEX page_idx 
ON tdspw_page_data (page_id, page_version, language);

CREATE TABLE tdspw_active_page (
	page_id VARCHAR(50) PRIMARY KEY,
	live_version INT NOT NULL,
	stage_version INT NOT NULL,
	created_at DATETIME NOT NULL,
	created_by VARCHAR(255) NOT NULL,
	updated_at DATETIME NULL,
	updated_by VARCHAR(255) NULL
);


---- Cleanup ----
DROP TABLE tdspw_active_page;

DROP INDEX page_idx ON tdspw_page_data;

DROP TABLE tdspw_page_data;

DROP INDEX status_idx ON tdspw_page_info;

DROP TABLE tdspw_page_info;

