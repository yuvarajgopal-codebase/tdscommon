package com.tdsecurities.common.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;
import static org.hamcrest.CoreMatchers.*;

import java.util.ArrayList;
import java.util.List;

import com.tdsecurities.common.auth.AuthenticationFacade;
import com.tdsecurities.common.data.ActivePage;
import com.tdsecurities.common.data.ActivePageRepository;
import com.tdsecurities.common.data.PageInfo;
import com.tdsecurities.common.data.PageInfoId;
import com.tdsecurities.common.data.PageInfoRepository;
import com.tdsecurities.common.data.TemplateInfoId;
import com.tdsecurities.common.data.TemplateInfoRepository;
import com.tdsecurities.common.dto.PageInfoDTO;
import com.tdsecurities.common.exception.PageExistsException;
import com.tdsecurities.common.exception.TemplateNotFoundException;

import org.junit.Test;
import org.mockito.Mockito;

public class PageServiceTest {

    @Test
    public void testGetPages_Success() throws Exception {
        System.out.println("Running testCreatePage_Success()");


        List<PageInfo> pageInfoResultList = new ArrayList<PageInfo>();
        PageInfo pageInfo = new PageInfo();
        pageInfoResultList.add(pageInfo);

        PageInfoRepository pageInfoRepositoryMock = Mockito.mock(PageInfoRepository.class);
        when(pageInfoRepositoryMock.findByActive(anyBoolean())).thenReturn(pageInfoResultList);
       
        PageService pageService = new PageService();
        pageService.pageInfoRepository = pageInfoRepositoryMock;

        List<PageInfoDTO> pageInfoDTOList = pageService.getPages();
        verify(pageInfoRepositoryMock, times(1)).findByActive(anyBoolean());
        assertNotNull(pageInfoDTOList);
        assertThat(pageInfoDTOList.size(), is(1));
    }

    @Test
    public void testCreatePage_Success() throws Exception {
        System.out.println("Running testCreatePage_Success()");

        PageInfoRepository pageInfoRepositoryMock = Mockito.mock(PageInfoRepository.class);
        when(pageInfoRepositoryMock.existsById(any(PageInfoId.class))).thenReturn(false);

        PageInfo mockPageInfo = new PageInfo();
        mockPageInfo.setId("TestPage");
        mockPageInfo.setTemplateId("TestTemplate");
        mockPageInfo.setNotes("This is a test");
        when(pageInfoRepositoryMock.save(any())).thenReturn(mockPageInfo); 

        ActivePageRepository activePageRepositoryMock = Mockito.mock(ActivePageRepository.class);
        when(activePageRepositoryMock.save(any())).thenReturn(new ActivePage());

        TemplateInfoRepository templateInfoRepositoryMock = Mockito.mock(TemplateInfoRepository.class);
        when(templateInfoRepositoryMock.existsById(any(TemplateInfoId.class))).thenReturn(true);

        AuthenticationFacade authenticationFacadeMock = Mockito.mock(AuthenticationFacade.class);
        when(authenticationFacadeMock.getAuthenticatedUserName()).thenReturn("MockUser");
        
        PageService pageService = new PageService();
        pageService.pageInfoRepository = pageInfoRepositoryMock;
        pageService.activePageRepository = activePageRepositoryMock;
        pageService.templateInfoRepository = templateInfoRepositoryMock;
        pageService.authenticationFacade = authenticationFacadeMock;

        PageInfoDTO createdPage = pageService.createPage("TestPage", "TestTemplate", "This is a test");
        verify(pageInfoRepositoryMock, times(1)).existsById(any());
        verify(activePageRepositoryMock, times(1)).save(any());
        verify(authenticationFacadeMock, times(2)).getAuthenticatedUserName();
        assertNotNull(createdPage);
        assertThat(createdPage.getId(), is("TestPage"));
        assertThat(createdPage.getTemplateId(), is("TestTemplate"));
    }

    @Test(expected = PageExistsException.class)
    public void testCreatePage_PageExists() throws Exception {
        System.out.println("Running testCreatePage_PageExists()");

        PageInfoRepository pageInfoRepositoryMock = Mockito.mock(PageInfoRepository.class);
        when(pageInfoRepositoryMock.existsById(any(PageInfoId.class))).thenReturn(true);
        when(pageInfoRepositoryMock.save(any())).thenReturn(new PageInfo()); 

        ActivePageRepository activePageRepositoryMock = Mockito.mock(ActivePageRepository.class);
        when(activePageRepositoryMock.save(any())).thenReturn(new ActivePage());

        TemplateInfoRepository templateInfoRepositoryMock = Mockito.mock(TemplateInfoRepository.class);
        when(templateInfoRepositoryMock.existsById(any(TemplateInfoId.class))).thenReturn(true);

        AuthenticationFacade authenticationFacadeMock = Mockito.mock(AuthenticationFacade.class);
        when(authenticationFacadeMock.getAuthenticatedUserName()).thenReturn("MockUser");
        
        PageService pageService = new PageService();
        pageService.pageInfoRepository = pageInfoRepositoryMock;
        pageService.activePageRepository = activePageRepositoryMock;
        pageService.templateInfoRepository = templateInfoRepositoryMock;
        pageService.authenticationFacade = authenticationFacadeMock;

        PageInfoDTO createdPage = pageService.createPage("TestPage", "TestTemplate", "This is a test");
    }

    @Test(expected = TemplateNotFoundException.class)
    public void testCreatePage_TemplateDoesNotExists() throws Exception {
        System.out.println("Running testCreatePage_TemplateDoesNotExists()");

        PageInfoRepository pageInfoRepositoryMock = Mockito.mock(PageInfoRepository.class);
        when(pageInfoRepositoryMock.existsById(any(PageInfoId.class))).thenReturn(false);
        when(pageInfoRepositoryMock.save(any())).thenReturn(new PageInfo()); 

        ActivePageRepository activePageRepositoryMock = Mockito.mock(ActivePageRepository.class);
        when(activePageRepositoryMock.save(any())).thenReturn(new ActivePage());

        TemplateInfoRepository templateInfoRepositoryMock = Mockito.mock(TemplateInfoRepository.class);
        when(templateInfoRepositoryMock.existsById(any(TemplateInfoId.class))).thenReturn(false);

        AuthenticationFacade authenticationFacadeMock = Mockito.mock(AuthenticationFacade.class);
        when(authenticationFacadeMock.getAuthenticatedUserName()).thenReturn("MockUser");
        
        PageService pageService = new PageService();
        pageService.pageInfoRepository = pageInfoRepositoryMock;
        pageService.activePageRepository = activePageRepositoryMock;
        pageService.templateInfoRepository = templateInfoRepositoryMock;
        pageService.authenticationFacade = authenticationFacadeMock;

        PageInfoDTO createdPage = pageService.createPage("TestPage", "TestTemplate", "This is a test");
    }
}


