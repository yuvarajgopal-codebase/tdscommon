package com.tdsecurities.common.component;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import com.tdsecurities.common.config.CommonConstants;

import com.tdsecurities.common.dto.TextWithButtonsDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * A section with text and a button.
 * 
 * TD Standards Ref: 5.3 Text With Buttons
 */
public class TextWithButtons implements BaseComponent {

    private static final Logger logger = LoggerFactory.getLogger(TextWithButtons.class);

    private TextWithButtonsDTO textWithButtonsDTO;
    private CommonConstants commonConstants;

    public TextWithButtons(TextWithButtonsDTO textWithButtonsDTO, CommonConstants commonConstants) {
        this.textWithButtonsDTO = textWithButtonsDTO;
        this.commonConstants = commonConstants;
    }

    @Override
    public String generateContent(Configuration freeMarkerConfiguration) {

        try {

            Map<String, Object> templateParams = new HashMap<>();

            Template template = freeMarkerConfiguration.getTemplate("component/text_with_buttons.ftl");
            templateParams.put("textWithButton", textWithButtonsDTO);
            templateParams.put("constants", commonConstants);

            StringWriter stringWriter = new StringWriter();
            template.process(templateParams, stringWriter);

            return stringWriter.toString();

        } catch (Exception e) {
            logger.error("Error generating text with buttons content: ", e);
            throw new RuntimeException("Error generating text with buttons content", e);
        }
        
	}

}