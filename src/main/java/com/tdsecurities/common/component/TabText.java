package com.tdsecurities.common.component;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import com.tdsecurities.common.config.CommonConstants;
import com.tdsecurities.common.dto.TabTextDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class TabText implements BaseComponent {

    private static final Logger logger = LoggerFactory.getLogger(Banner.class);

    private TabTextDTO tabTextDTO;
    private CommonConstants commonConstants;

    public TabText(TabTextDTO tabTextDTO, CommonConstants commonConstants) {
        this.tabTextDTO = tabTextDTO;
        this.commonConstants = commonConstants;
    }

    @Override
    public String generateContent(Configuration freeMarkerConfiguration) {

        try {

            Map<String, Object> templateParams = new HashMap<>();

            Template template = freeMarkerConfiguration.getTemplate("component/tab_text.ftl");
            templateParams.put("tabText", tabTextDTO);
            templateParams.put("constants", commonConstants);

            StringWriter stringWriter = new StringWriter();
            template.process(templateParams, stringWriter);

            return stringWriter.toString();

        } catch (Exception e) {
            logger.error("Error generating image copy content: ", e);
            throw new RuntimeException("Error generating image copy content", e);
        }
        
	}

}