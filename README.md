## tdscommon
TD Securities Public Website - Common Files

Shared files between the TDS public webiste and the TDS admin application

### Build
mvn clean install

### Maven Repo
The TD Nexus repo should be used as the main repository. This can be configured in your Maven settings.xml file. The following is the sample configuration.

```
    <mirrors>
        <mirror>
            <id>td-nexus</id>
            <name>Public</name>
            <url>https://repo.td.com/repository/maven-public/</url>
            <mirrorOf>central</mirrorOf>
        </mirror>
    </mirrors>
```