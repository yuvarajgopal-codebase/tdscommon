package com.tdsecurities.common.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tdsecurities.common.auth.AuthenticationFacade;
import com.tdsecurities.common.config.CommonConstants;
import com.tdsecurities.common.data.ActivePage;
import com.tdsecurities.common.data.ActivePageRepository;
import com.tdsecurities.common.data.PageData;
import com.tdsecurities.common.data.PageDataRepository;
import com.tdsecurities.common.data.PageInfo;
import com.tdsecurities.common.data.PageInfoId;
import com.tdsecurities.common.data.PageInfoRepository;
import com.tdsecurities.common.data.TemplateComponent;
import com.tdsecurities.common.data.TemplateComponentRepository;
import com.tdsecurities.common.data.TemplateInfo;
import com.tdsecurities.common.data.TemplateInfoId;
import com.tdsecurities.common.data.TemplateInfoRepository;
import com.tdsecurities.common.data.WorkflowStatus;
import com.tdsecurities.common.dto.PageEditDTO;
import com.tdsecurities.common.dto.PageInfoDTO;
import com.tdsecurities.common.dto.PageUpdateDTO;
import com.tdsecurities.common.exception.PageDataParsingException;
import com.tdsecurities.common.exception.PageNotEditableException;
import com.tdsecurities.common.exception.PageNotFoundException;
import com.tdsecurities.common.exception.TemplateNotFoundException;
import com.tdsecurities.common.util.TransformUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PageEditService {

    private static final Logger logger = LoggerFactory.getLogger(PageEditService.class);

    @Autowired
    CommonConstants commonConstants;

    @Autowired
    PageInfoRepository pageInfoRepository;

    @Autowired
    PageDataRepository pageDataRepository;

    @Autowired
    ActivePageRepository activePageRepository;
    
    @Autowired
    TemplateInfoRepository templateInfoRepository;

    @Autowired
    TemplateComponentRepository templateComponentRepository;

    @Autowired
    ComponentService componentService;

    @Autowired
    AuthenticationFacade authenticationFacade;

    public PageEditDTO getPageToEdit(String pageId, String language) {

        logger.info("Processing page edit data for page {} with language {}", pageId, language);

        PageEditDTO pageEditDTO = new PageEditDTO();

        PageInfo pageInfo = getEditableVersion(pageId);

        pageEditDTO.setPageInfo(TransformUtils.xfromPageInfo(pageInfo));

        // Get the Template info
        Optional<TemplateInfo> templateInfoOptional = templateInfoRepository.findById(
            new TemplateInfoId(pageInfo.getTemplateId(), pageInfo.getTemplateVersion()));

        if (!templateInfoOptional.isPresent()) {
            logger.warn("Could not find Template {} with Version {}", pageInfo.getTemplateId(), pageInfo.getTemplateVersion());
            throw new TemplateNotFoundException("Could not find template for page to edit");
        }

        TemplateInfo templateInfo = templateInfoOptional.get();
        pageEditDTO.setTemplateInfo(TransformUtils.xformTemplateInfo(templateInfo));

        // Get the Template components
        List<TemplateComponent> templateComponentList = templateComponentRepository.findByTemplateIdAndTemplateVersionOrderBySortOrder(templateInfo.getId(), templateInfo.getVersion());
        pageEditDTO.setTemplateComponents(TransformUtils.xfromTemplateComponents(templateComponentList));

        // Get the Page Data
        List<PageData> pageDataList = pageDataRepository.findByPageIdAndPageVersionAndLanguage(
            pageInfo.getId(), pageInfo.getVersion(), language);


        if (pageDataList != null && pageDataList.size() >= 1) {
            PageData pageData = pageDataList.get(0);

            Map<String, Object> pageDataComponents = getPageDataMap(templateComponentList, pageData.getComponentData());
            pageEditDTO.setPageData(pageDataComponents);
        }

        pageEditDTO.setLanguage(language);

        // Return the DTO with the data
        return pageEditDTO;

    }

    private Map<String, Object> getPageDataMap(List<TemplateComponent> templateComponentList, 
        String componentData) {

        Map<String, Object> pageDataComponents = new HashMap<>();

        JsonParser parser = new JsonParser();
        JsonElement rootElement = parser.parse(componentData);

        if (rootElement.isJsonObject()) {
            JsonObject rootObject = rootElement.getAsJsonObject();

            for (TemplateComponent templateComponent: templateComponentList) {

                logger.info("Looking for component {}", templateComponent.getName());
                JsonElement componentElement = rootObject.get(templateComponent.getName());

                if (componentElement != null) {
                    Object component = componentService.getComponentFromJson(templateComponent.getComponentType(), componentElement);

                    if (component != null) {
                        pageDataComponents.put(templateComponent.getName(), component);
                    }
                }
            }

        } else {
            logger.error("Error parsing page data");
            throw new PageDataParsingException("Error parsing page data");
        }

        return pageDataComponents;
    }

    @Transactional
    public void updateEditedPage(String pageId, String language, PageUpdateDTO pageUpdateDTO) {

        logger.info("Processing page data update for page {} and language {}", pageId, language);

        // TODO: Validate input data (against DTOs to make sure that they are valid)

        // Get the PageInfo to update.
        PageInfo pageInfo = getEditableVersion(pageId);

        // Thrown an error if an attempt was made to update a page that was not in Draft or Published state
        if (!pageInfo.getStatus().equals(WorkflowStatus.Draft) && !pageInfo.getStatus().equals(WorkflowStatus.Published)) {
            logger.warn("Page not updatable. There is no version of the page {} that currently supports updates", pageId);
            throw new PageNotEditableException("Page not updatable. There is no version of the page that currently supports updates");
        }

        logger.info("Found editable page info {} with version {} and status {}", pageInfo.getId(), 
            pageInfo.getVersion(), pageInfo.getStatus());

        PageInfo pageInfoToUpdate = null;

        // If the status is Published, then create a new version of the page with Draft status
        if (pageInfo.getStatus().equals(WorkflowStatus.Published)) {
            logger.info("Creating new page info version since current version is Published");

            long highestVersion = pageInfoRepository.getMaxVersion(pageInfo.getId());
            pageInfoToUpdate = new PageInfo();
            pageInfoToUpdate.setId(pageInfo.getId());
            pageInfoToUpdate.setVersion(highestVersion + 1);
            pageInfoToUpdate.setTemplateId(pageInfo.getTemplateId());
            pageInfoToUpdate.setTemplateVersion(pageInfo.getTemplateVersion());
            pageInfoToUpdate.setNotes(pageInfo.getNotes());
            pageInfoToUpdate.setStatus(WorkflowStatus.Draft);
            pageInfoToUpdate.setActive(true);
            pageInfoToUpdate.setCreatedAt(new Date());
            pageInfoToUpdate.setCreatedBy(authenticationFacade.getAuthenticatedUserName());
        // Otherwise update the page that is already in Draft status
        } else {
            logger.info("Updating exsiting page info version");
            pageInfoToUpdate = pageInfo;
            pageInfoToUpdate.setUpdatedAt(new Date());
            pageInfoToUpdate.setUpdatedBy(authenticationFacade.getAuthenticatedUserName());
        }

        // Update PageInfo including status
        PageInfoDTO pageInfoDTO = pageUpdateDTO.getPageInfo();

        if (pageInfoDTO != null) {
            pageInfoToUpdate.setNotes(pageInfoDTO.getNotes());     
        }

        pageInfoRepository.save(pageInfoToUpdate);

        List<PageData> pageDataList = pageDataRepository.findByPageIdAndPageVersionAndLanguage(
            pageInfoToUpdate.getId(), pageInfoToUpdate.getVersion(), language);

        PageData pageData = null;

        if (pageDataList == null || pageDataList.isEmpty()) {
            logger.info("Could not find existing page data. Creating a new one");
            pageData = new PageData();
            pageData.setPageId(pageInfoToUpdate.getId());
            pageData.setPageVersion(pageInfoToUpdate.getVersion());
            pageData.setLanguage(language);
            pageData.setCreatedAt(new Date());
            pageData.setCreatedBy(authenticationFacade.getAuthenticatedUserName());
        } else {
            logger.info("Replacing existing page data.");
            pageData = pageDataList.get(0);
            pageData.setUpdatedAt(new Date());
            pageData.setUpdatedBy(authenticationFacade.getAuthenticatedUserName());
        }

        // Update/Insert PageData.
        Gson gson = new Gson();
        String serializedComponentData = gson.toJson(pageUpdateDTO.getPageData());
        pageData.setComponentData(serializedComponentData);
        pageData.setTitle(pageUpdateDTO.getTitle());
        pageData.setAnalyticsTag(pageUpdateDTO.getAnalyticsTag());
        pageData.setSeoMetadata(pageUpdateDTO.getSeoMetadata());
        pageDataRepository.save(pageData);
        
        logger.info("Successfully updated page data");

    }

    /**
     * Get the PageInfo version to edit.
     * If there is a PageInfo with a 'Draft' status, then return this version
     * If there is a PageInfo with the 'Submitted' or 'Stage' status, then throw an error as only
     *      one version of a page can be in the work
     * If there is a PageInfo with a 'Published' status, then return this version
     */
    private PageInfo getEditableVersion(String pageId) {

        // Check for draft version of the page
        List<PageInfo> draftPageInfoList = pageInfoRepository.findByIdAndStatusAndActive(
            pageId, WorkflowStatus.Draft, true);

        if (draftPageInfoList != null && draftPageInfoList.size() > 1) {
            logger.warn("Status mismatch. There are multiple versions of page {} with Draft status", pageId);
            throw new PageNotEditableException("Status mismatch. There are multiple versions of this page with Draft status");
        }

        if (draftPageInfoList != null && draftPageInfoList.size() == 1) {
            return draftPageInfoList.get(0);
        }

        // Check for Submitted or Stage sattus
        List<PageInfo> submittedPageInfoList = pageInfoRepository.findByIdAndStatusAndActive(
            pageId, WorkflowStatus.Submitted, true);

        if (submittedPageInfoList != null && submittedPageInfoList.size() > 0) {
            logger.warn("Page not editable. There is already a version of the page {} with Submitted status", pageId);
            throw new PageNotEditableException("Page not editable. There is already a version of the page with Submitted status");
        }

        List<PageInfo> stagePageInfoList = pageInfoRepository.findByIdAndStatusAndActive(
            pageId, WorkflowStatus.Stage, true);

        if (stagePageInfoList != null && stagePageInfoList.size() > 0) {
            logger.warn("Page not editable. There is already a version of the page {} with Stage status", pageId);
            throw new PageNotEditableException("Page not editable. There is already a version of the page with Stage status");
        }

        // Get the current live page to edit
        Optional<ActivePage> activePageOptional = activePageRepository.findById(pageId);
        if (!activePageOptional.isPresent()) {
            logger.warn("Page not editable. Could not find the page {}", pageId);
            throw new PageNotFoundException("Page not editable. Could not find the page");
        }

        ActivePage activePage = activePageOptional.get();

        if (activePage.getLiveVersion() == 0) {
            logger.warn("Page not editable. Could not find a published version of the page {} to edit", pageId);
            throw new PageNotEditableException("Page not editable. Could not find a published version of the page to edit");    
        }
    
        // Get the Page Info
        Optional<PageInfo> pageInfoOptional = pageInfoRepository.findById(
            new PageInfoId(pageId, activePage.getLiveVersion()));
            
        if (!pageInfoOptional.isPresent()) {
            logger.warn("Could not find page {} with version {} to edit", pageId, activePage.getLiveVersion());
            throw new PageNotFoundException("Could not find page to edit");
        }

        PageInfo pageInfo = pageInfoOptional.get();

        // Verify that the page is published and active
        if (pageInfo.getStatus().equals(WorkflowStatus.Published) && pageInfo.isActive() == true) {
            return pageInfo;
        } else {
            logger.warn("Could not find any version of page {} to edit", pageId);
            throw new PageNotFoundException("Could not find any version of the page to edit");
        }

    }
}