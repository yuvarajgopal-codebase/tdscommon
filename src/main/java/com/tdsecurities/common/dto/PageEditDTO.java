package com.tdsecurities.common.dto;

import java.util.List;
import java.util.Map;

/**
 * Contains the information needed to edit a page
 */
public class PageEditDTO {

    private PageInfoDTO pageInfo;
    private String language;
    private String title;
    private String analyticsTag;
    private String seoMetadata;
    private Map<String, Object> pageData;
    private TemplateInfoDTO templateInfo;
    private List<TemplateComponentDTO> templateComponents;

    public PageInfoDTO getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfoDTO pageInfo) {
        this.pageInfo = pageInfo;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnalyticsTag() {
        return analyticsTag;
    }

    public void setAnalyticsTag(String analyticsTag) {
        this.analyticsTag = analyticsTag;
    }

    public String getSeoMetadata() {
        return seoMetadata;
    }

    public void setSeoMetadata(String seoMetadata) {
        this.seoMetadata = seoMetadata;
    }

    public Map<String, Object> getPageData() {
        return pageData;
    }

    public void setPageData(Map<String, Object> pageData) {
        this.pageData = pageData;
    }

    public TemplateInfoDTO getTemplateInfo() {
        return templateInfo;
    }

    public void setTemplateInfo(TemplateInfoDTO templateInfo) {
        this.templateInfo = templateInfo;
    }

    public List<TemplateComponentDTO> getTemplateComponents() {
        return templateComponents;
    }

    public void setTemplateComponents(List<TemplateComponentDTO> templateComponents) {
        this.templateComponents = templateComponents;
    }

    @Override
    public String toString() {
        return "PageEditDTO [analyticsTag=" + analyticsTag + ", language=" + language + ", pageData=" + pageData
                + ", pageInfo=" + pageInfo + ", seoMetadata=" + seoMetadata + ", templateComponents="
                + templateComponents + ", templateInfo=" + templateInfo + ", title=" + title + "]";
    }

}