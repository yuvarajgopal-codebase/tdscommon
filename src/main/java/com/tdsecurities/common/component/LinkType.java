package com.tdsecurities.common.component;

public enum LinkType {
    Anchor,
    Button
}