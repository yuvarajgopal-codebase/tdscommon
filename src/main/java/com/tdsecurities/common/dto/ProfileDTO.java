package com.tdsecurities.common.dto;

public class ProfileDTO {

    private String imageId;
    private String imageAlt;

    private String name;
    private String title;
    private String description;
    private String profilePageId;

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImageAlt() {
        return imageAlt;
    }

    public void setImageAlt(String imageAlt) {
        this.imageAlt = imageAlt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfilePageId() {
        return profilePageId;
    }

    public void setProfilePageId(String profilePageId) {
        this.profilePageId = profilePageId;
    }

    @Override
    public String toString() {
        return "ProfileDTO [description=" + description + ", imageAlt=" + imageAlt + ", imageId=" + imageId + ", name="
                + name + ", profilePageId=" + profilePageId + ", title=" + title + "]";
    }

}