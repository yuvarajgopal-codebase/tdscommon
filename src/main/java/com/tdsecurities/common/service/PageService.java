package com.tdsecurities.common.service;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tdsecurities.common.auth.AuthenticationFacade;
import com.tdsecurities.common.component.Footer;
import com.tdsecurities.common.component.Header;
import com.tdsecurities.common.config.CommonConstants;
import com.tdsecurities.common.data.ActivePage;
import com.tdsecurities.common.data.ActivePageRepository;
import com.tdsecurities.common.data.PageData;
import com.tdsecurities.common.data.PageDataRepository;
import com.tdsecurities.common.data.PageInfo;
import com.tdsecurities.common.data.PageInfoId;
import com.tdsecurities.common.data.PageInfoRepository;
import com.tdsecurities.common.data.TemplateComponent;
import com.tdsecurities.common.data.TemplateComponentRepository;
import com.tdsecurities.common.data.TemplateInfo;
import com.tdsecurities.common.data.TemplateInfoId;
import com.tdsecurities.common.data.TemplateInfoRepository;
import com.tdsecurities.common.data.WorkflowStatus;
import com.tdsecurities.common.dto.PageInfoDTO;
import com.tdsecurities.common.exception.PageDataParsingException;
import com.tdsecurities.common.exception.PageExistsException;
import com.tdsecurities.common.exception.PageGenerationException;
import com.tdsecurities.common.exception.PageNotFoundException;
import com.tdsecurities.common.exception.TemplateNotFoundException;
import com.tdsecurities.common.util.TransformUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import freemarker.template.Configuration;
import freemarker.template.Template;

@Service
public class PageService {

    private static final Logger logger = LoggerFactory.getLogger(PageService.class);

    @Autowired
    CommonConstants commonConstants;

    @Autowired
    Configuration freeMarkerConfiguration;

    @Autowired
    PageInfoRepository pageInfoRepository;

    @Autowired
    PageDataRepository pageDataRepository;
    
    @Autowired
    TemplateInfoRepository templateInfoRepository;

    @Autowired
    TemplateComponentRepository templateComponentRepository;

    @Autowired
    ActivePageRepository activePageRepository;

    @Autowired
    ComponentService componentService;

    @Autowired
    AuthenticationFacade authenticationFacade;

    public List<PageInfoDTO> getPages() {
        
        logger.info("Getting list of PageInfo");

        List<PageInfoDTO> pageInfoResultList = new ArrayList<PageInfoDTO>();
        
        List<PageInfo> pageInfoList = pageInfoRepository.findByActive(true);

        logger.info("Found {} PageInfo", (pageInfoList != null ? pageInfoList.size(): 0));

        for (PageInfo pageInfo: pageInfoList) {
            PageInfoDTO pageInfoDTO = TransformUtils.xfromPageInfo(pageInfo);

            if (pageInfoDTO != null) {
                pageInfoResultList.add(pageInfoDTO);
            }
        }

        return pageInfoResultList;

    }

    public boolean pageExists(String pageId) {
        logger.info("Checking if Page {} exists", pageId);

        // Get the Page Info
        List<PageInfo> pageInfoList = pageInfoRepository.findByIdAndActive(pageId, true);
            
        if (pageInfoList != null && pageInfoList.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Looks for the page and returns the HTML version of the page (based on the page template)
     * NOTE: if the page is not found in the language specified, the default language version of the page is returned.
     * 
     * @param pageId
     * @param version
     * @param language
     * @return
     *      HTML version of the page
     *      null if the page/version cannot be found
     */
    public String getPageContent(String pageId, long version, String language) {

        logger.info("Processing request for Page {} Version {} Language {}", pageId, version, language);

        try {
            // Get the Page Info
            Optional<PageInfo> pageInfoOptional = pageInfoRepository.findById(
                new PageInfoId(pageId, version));
                
            if (!pageInfoOptional.isPresent()) {
                logger.warn("Cound not find Page {} with Version {}", pageId, version);
                throw new PageNotFoundException("Could not find page");
            }

            PageInfo pageInfo = pageInfoOptional.get();
            logger.info("Found Page {}", pageInfo.getId());

            // Get the Page Data
            List<PageData> pageDataList = pageDataRepository.findByPageIdAndPageVersionAndLanguage(
                pageInfo.getId(), pageInfo.getVersion(), language);

            
            if (pageDataList.isEmpty()) {

                // Fallback to the default language
                if (language.equals(commonConstants.getDefaultLanguage()) == false) {
                    pageDataList = pageDataRepository.findByPageIdAndPageVersionAndLanguage(
                        pageInfo.getId(), pageInfo.getVersion(), commonConstants.getDefaultLanguage());
                }
            }

            if (pageDataList.isEmpty()) {
                logger.warn("No data found for page {}", pageInfo.getId());
                throw new PageDataParsingException("No page data found for page");
            }

            // We're only interested in the first entry (there should only be one returned)
            PageData pageData = pageDataList.get(0);

            // Generate the page content (not including standard headers/footers etc.)
            String pageContent = generatePageContent(pageInfo, pageData);

            if (pageContent == null) {
                logger.warn("Generated page content is empty");
                throw new PageGenerationException("Generated page content is empty");
            }

            // Assemble the page
            Template mainTemplate = freeMarkerConfiguration.getTemplate("main/main.ftl");
            Map<String, Object> mainPageParams = new HashMap<>();

            mainPageParams.put("pageTitle", pageData.getTitle());
            mainPageParams.put("analyticsTag", pageData.getAnalyticsTag());
            mainPageParams.put("seoMetadata", pageData.getSeoMetadata());

            Header header = new Header(commonConstants);
            mainPageParams.put("header", header.generateContent(freeMarkerConfiguration));

            Footer footer = new Footer(commonConstants);
            mainPageParams.put("footer", footer.generateContent(freeMarkerConfiguration));

            mainPageParams.put("pageContent", pageContent);

            StringWriter mainPageStringWriter = new StringWriter();
            mainTemplate.process(mainPageParams, mainPageStringWriter);

            return mainPageStringWriter.toString();

        } catch (Exception e) {

            logger.error("Error building Page {} with Version {}", pageId, version, e);
            throw new PageGenerationException("Error generating page content");
            
        }   

    }

    // Generate Page Content
    private String generatePageContent(PageInfo pageInfo, PageData pageData) throws Exception {

        // Get template for page
        Optional<TemplateInfo> templateInfoOptional = templateInfoRepository.findById(
            new TemplateInfoId(pageInfo.getTemplateId(), pageInfo.getTemplateVersion()));

        if (!templateInfoOptional.isPresent()) {
            logger.warn("Cound not find Template {} with Version {}", pageInfo.getTemplateId(), pageInfo.getTemplateVersion());
            throw new TemplateNotFoundException("Could not find template");
        }

        TemplateInfo templateInfo = templateInfoOptional.get();
        logger.info("Using template {}", templateInfo.getTemplateFile());
        Template pageTemplate = freeMarkerConfiguration.getTemplate(String.format("page/%s.ftl", templateInfo.getTemplateFile()));

        // Get template components
        List<TemplateComponent> templateComponents = templateComponentRepository.findByTemplateIdAndTemplateVersionOrderBySortOrder(templateInfo.getId(), templateInfo.getVersion());
        logger.info("Got {} template components", templateComponents.size());

        Map<String, Object> pageParams = new HashMap<>();

        JsonParser parser = new JsonParser();
        JsonElement rootElement = parser.parse(pageData.getComponentData());

        if (rootElement.isJsonObject()) {
            JsonObject rootObject = rootElement.getAsJsonObject();

            for (TemplateComponent templateComponent: templateComponents) {

                logger.info("Looking for component {}", templateComponent.getName());
                JsonElement componentElement = rootObject.get(templateComponent.getName());

                if (componentElement != null) {

                    logger.info("Found component {}", templateComponent.getName());
                    String generatedComponentContent = componentService.generateContentForComponent(
                        templateComponent.getComponentType(), componentElement);

                    if (generatedComponentContent != null) {
                        pageParams.put(templateComponent.getName(), generatedComponentContent);
                    }
                }
            }

        } else {
            logger.error("Error parsing page data for page {}", pageInfo.getId());
            throw new PageDataParsingException("Error parsing page data");
        }

        // Process page template
        StringWriter pageStringWriter = new StringWriter();
        pageTemplate.process(pageParams, pageStringWriter);
        return pageStringWriter.toString();

    }

    @Transactional
    public PageInfoDTO createPage(String pageId, String templateId, String notes) {
        
        logger.info("Attempting to create a page with ID {} and template ID {}", pageId, templateId);

        // Check if pageId already exists
        if (pageInfoRepository.existsById(new PageInfoId(pageId, 1))) {
            logger.warn("Cannot create page. Page with ID {} and version {} already exists", pageId, 1);
            throw new PageExistsException("Page already exists");
        }
        
        // Validate that the template exists
        if (templateInfoRepository.existsById(new TemplateInfoId(templateId, 1)) == false) {
            logger.warn("Cannot create page. Template with ID {} and version {} was not found", templateId, 1);
            throw new TemplateNotFoundException("Error during page creation. Template was not found.");
        }

        // Save new PageInfo to the database
        logger.info("Page doesn't exist. Preparing to create it");

        PageInfo pageInfo = new PageInfo();
        pageInfo.setId(pageId);
        pageInfo.setVersion(1);
        pageInfo.setTemplateId(templateId);
        pageInfo.setTemplateVersion(1);
        pageInfo.setNotes(notes);
        pageInfo.setStatus(WorkflowStatus.Draft);
        pageInfo.setActive(true);
        pageInfo.setCreatedAt(new Date());
        pageInfo.setCreatedBy(authenticationFacade.getAuthenticatedUserName());
        PageInfo createdPageInfo = pageInfoRepository.save(pageInfo);

        // Save new ActivePage to the database
        ActivePage activePage = new ActivePage();
        activePage.setPageId(pageId);
        activePage.setLiveVersion(0);
        activePage.setStageVersion(0);
        activePage.setCreatedAt(new Date());
        activePage.setCreatedBy(authenticationFacade.getAuthenticatedUserName());
        activePageRepository.save(activePage);

        PageInfoDTO pageInfoDTO = TransformUtils.xfromPageInfo(createdPageInfo);

        return pageInfoDTO;
    }
}