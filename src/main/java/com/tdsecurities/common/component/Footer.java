package com.tdsecurities.common.component;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import com.tdsecurities.common.config.CommonConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class Footer implements BaseComponent {

    private static final Logger logger = LoggerFactory.getLogger(Footer.class);

    private CommonConstants commonConstants;

    public Footer(CommonConstants commonConstants) {
        this.commonConstants = commonConstants;
    }

    @Override
    public String generateContent(Configuration freeMarkerConfiguration) {

        try {

            Map<String, Object> templateParams = new HashMap<>();

            Template template = freeMarkerConfiguration.getTemplate("main/footer.ftl");
            templateParams.put("constants", commonConstants);

            StringWriter stringWriter = new StringWriter();
            template.process(templateParams, stringWriter);

            return stringWriter.toString();

        } catch (Exception e) {
            logger.error("Error generating footer content: ", e);
            throw new RuntimeException("Error generating footer content", e);
        }
        
	}

}