package com.tdsecurities.common.data;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PageInfoRepository extends CrudRepository<PageInfo, PageInfoId> {

    List<PageInfo> findAll();
    List<PageInfo> findByActive(boolean active);
    List<PageInfo> findByIdAndStatusAndActive(String id, WorkflowStatus workflow, boolean active);
    List<PageInfo> findByIdAndActive(String id, boolean active);

    @Query(value = "SELECT max(version) FROM PageInfo where id = ?1")
    long getMaxVersion(String id);
}