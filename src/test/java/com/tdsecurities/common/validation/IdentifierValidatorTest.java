package com.tdsecurities.common.validation;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;

public class IdentifierValidatorTest {

    @Test
    public void testIsValid_validIdentifier() {
        IdentifierValidator identifierValidator = new IdentifierValidator();
        boolean isValid = identifierValidator.isValid("Test123", null);
        assertThat(isValid, is(true));
    }

    @Test
    public void testIsValid_invalidCharacters() {
        IdentifierValidator identifierValidator = new IdentifierValidator();
        boolean isValid = identifierValidator.isValid("Test$123", null);
        assertThat(isValid, is(false));
    }

}