        <!--start of tab-->
        <section class="td_rq_tabs-carousel td-tab-with-label">
            <div class=" td-container td-gtb-text">
                <div class="td-row">
                    <div class="td-col-xs-12">
                        <h2>${tabText.tabHeader}</h2>
                    </div>
                </div>
            </div>
            <div class="td-tabs-carousel-container td-tab-with-label td-bg-light-gray">
                <span class="accessibility-instructions">Tabs Menu: to navigate this menu, use the left & right arrow keys to change tabs. Press tab to go into the content. Shift-tab to return to the tabs.</span>
                <ul class="td-tabs-carousel">
                    <#list tabText.tabs as tabItem>
                        <li id="Tab_1" class="td-tabs-carousel-tab">
                            <div class="td-tabs-carousel-tab-content">
                                <h4 class="label">${tabItem.tabTitle}</h4>
                            </div>
                        </li>
                    </#list>
                 </ul>
                <#list tabText.tabs as tabItem> 
                <div class="td-tabs-carousel-content" id="tabsCarousel0_tab0" role="tabpanel" aria-hidden="false" style="display: block;">
                    <div class="td-contentarea">
                        <section class="td_rq_table-image-left td-image-block-with-copy td-image-block-left-with-background td-background-image">
                            <div class="td-col td-col-sm-12">
                                <div class="vis">
                                    <div class="td-col td-col-xs-0 td-col-xs-offset-0 td-col-sm-0 td-col-sm-offset-0 td-col-md-offset-6 td-col-md-6 td-col-lg-6 td-col-lg-offset-6 td-extend-left td-extend-right">
                                        <picture class="td-underwriting-image">
                                        <source srcset="${constants.imageContext}/${tabItem.imageId}?size=lg" media="(min-width: 1200px)">
                                        <source srcset="${constants.imageContext}/${tabItem.imageId}?size=md" media="(min-width: 1024px)">
                                        <!-- Following is a fallback for IE. It makes sense to serve the lg image because we know it's a desktop -->
                                        <img srcset="${constants.imageContext}/${tabItem.imageId}?size=sm" alt="" media="(max-width: 1024px)">
                                    </picture>
                                    </div>
                                </div>

                                <div>
                                    <div class="td-row">
                                        <div>
                                            <!-- copy col -->
                                            <div class="td-col td-col-xs-12 td-col-xs-offset-0 td-col-sm-12 td-col-sm-offset-0 td-col-md-6 td-col-md-offset-0 td-col-lg-5 td-col-lg-offset-1 td-copy-container">

                                                <div class="td-container">
                                                    <div class="td-left-image-text">
                                                        <div>
                                                            <div class="rte td-text-copy-gtb">
                                                                <ul class="td-rte-margin-top-none">
                                                                    <h2 class="td-text-copy-gtb">${tabItem.title}</h2>
                                                                    <p>
                                                                        ${tabItem.textBlock}
                                                                    </p>
                                                                </ul>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </section>
                        <!-- end of left text Image-->
                        <!--Section with two columns-->
                        <section class="global-transaction-page td_bullet_list td-gtb-bullet-text ">
                            <div class="">

                                <div class="td-row">
                                    <div class="td-col-xs-12">
                                        <h2>${(tabItem.bottomTitle)!}</h2>
                                    </div>
                                </div>


                                <div class="td-row td-gtb-li-row">
                                    <div class="td-col-xs-12 td-col-sm-5 td-col-sm-offset-1 td-col-lg-5 td-col-lg-offset-0 list">
                                        <div class="rte">
                                            ${(tabItem.bottomTextLeft)!}
                                        </div>
                                    </div>
                                    <div class="td-col-xs-12 td-col-sm-5 td-col-lg-5 td-col-lg-offset-1 list">
                                        <div class="rte">
                                             ${(tabItem.bottomTextRight)!}
                                        </div>
                                    </div>
                                </div>
                                <div class="td-row">
                                    <div class="td-col-xs-12 link-align">

                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                </#list>
               
            </div>

        </section>

        <!--end of tab-->