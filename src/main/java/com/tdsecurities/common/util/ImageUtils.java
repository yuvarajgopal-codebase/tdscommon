package com.tdsecurities.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.util.InvalidMimeTypeException;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;

public class ImageUtils {

    private static final Logger logger = LoggerFactory.getLogger(ImageUtils.class);

    public static MediaType getMediaTypeForMimeType(String mimeTypeString) {

        try {
            MimeType mimeType = MimeTypeUtils.parseMimeType(mimeTypeString);
            MediaType mediaType = MediaType.asMediaType(mimeType);
            return mediaType;

        } catch (InvalidMimeTypeException invalidMimeTypeEx) {
            logger.warn("Error converting mime type value to MediaType");
            return null;
        }

    }
}