package com.tdsecurities.common.dto;

public class TextWithButtonsDTO {

    private String title;
    private LinkDTO button;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LinkDTO getButton() {
        return button;
    }

    public void setButton(LinkDTO button) {
        this.button = button;
    }

    @Override
    public String toString() {
        return "TextWithButtonsDTO [button=" + button + ", title=" + title + "]";
    }

}