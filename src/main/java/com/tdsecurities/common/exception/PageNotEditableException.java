package com.tdsecurities.common.exception;

public class PageNotEditableException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public PageNotEditableException() {
    }

    public PageNotEditableException(String message) {
        super(message);
    }

    public PageNotEditableException(Throwable cause) {
        super(cause);
    }

    public PageNotEditableException(String message, Throwable cause) {
        super(message, cause);
    }

}