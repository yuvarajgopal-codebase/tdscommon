package com.tdsecurities.common.data;

public enum WorkflowStatus {
    Draft,
    Submitted,
    Stage,
    Published,
    UnPublished
}