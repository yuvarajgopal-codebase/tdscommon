package com.tdsecurities.common.dto;

public class BannerDTO {

    private String title;
    private String imageId;
    private String description;
    private LinkDTO ctaButton;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LinkDTO getCtaButton() {
        return ctaButton;
    }

    public void setCtaButton(LinkDTO ctaButton) {
        this.ctaButton = ctaButton;
    }

    @Override
    public String toString() {
        return "BannerDTO [ctaButton=" + ctaButton + ", description=" + description + ", imageId=" + imageId
                + ", title=" + title + "]";
    }



    
}