package com.tdsecurities.common.component;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tdsecurities.common.config.CommonConstants;
import com.tdsecurities.common.dto.ArticleDTO;
import com.tdsecurities.common.dto.ArticleListDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class ArticleList extends CompositeComponent<Article> implements BaseComponent {

    private static final Logger logger = LoggerFactory.getLogger(ArticleList.class);

    private ArticleListDTO articleListDTO;
    private CommonConstants commonConstants;

    public ArticleList(ArticleListDTO articleListDTO, CommonConstants commonConstants) {
        this.articleListDTO = articleListDTO;
        this.commonConstants = commonConstants;
    }

    @Override
    public String generateContent(Configuration freeMarkerConfiguration) {

        try {
            if (articleListDTO == null) {
                return null;
            }

            Map<String, Object> templateParams = new HashMap<>();

            Template template = freeMarkerConfiguration.getTemplate("component/article_list.ftl");
            templateParams.put("title", articleListDTO.getTitle());
            templateParams.put("loadMoreText", articleListDTO.getLoadMoreText());

            if (articleListDTO.getArticles() != null) {
                for (ArticleDTO articleDTO: articleListDTO.getArticles()) {
                    add(new Article(articleDTO, commonConstants));
                }
            }

            List<String> articleContentList = getGeneratedComponentContent(freeMarkerConfiguration);
            templateParams.put("articles", articleContentList);

            StringWriter stringWriter = new StringWriter();
            template.process(templateParams, stringWriter);

            return stringWriter.toString();

        } catch (Exception e) {
            logger.error("Error generating article list content: ", e);
            throw new RuntimeException("Error generating article list content", e);
        }

	}

}