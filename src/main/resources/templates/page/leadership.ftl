    <#if leadershipBanner?has_content>
    ${leadershipBanner}
    </#if>

    <#if leadershipGrid?has_content>
        <div class="alternate-bg-color-sections">
        ${leadershipGrid}
        </div>
    </#if>
    
    <#if fromLocalToGlobal?has_content>
        <div class="alternate-bg-color-sections">
        ${fromLocalToGlobal}
        </div>
    </#if>

    <#if articleList?has_content>
        <div class="alternate-bg-color-sections">
        ${articleList}
        </div>
    </#if>
