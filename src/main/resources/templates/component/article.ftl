            <li class="td-row">
                <div class="hidden-xs td-col-sm-3 td-col-sm-offset-1 td-col-lg-3 td-col-lg-offset-2">
                    <div class="td-lazy">
                        <a href="${constants.pageContext}/${article.articlePageId}" aria-hidden="true" tabindex="-1"><img class="img-responsive" alt="${article.imageAlt}" src="/assets/img/spinner/loading-spinner-small.gif" data-src="${constants.imageContext}/${article.imageId}"/></a>
                        <!--<a href="${constants.pageContext}/${article.articlePageId}" aria-hidden="true" tabindex="-1"><img class="img-responsive" alt="" src="/assets/img/spinner/loading-spinner-small.gif" data-src="/assets/img/article-list/dummy1.jpg"/></a>-->
                    </div>
                </div>
                <div class="td-col-xs-12 td-col-sm-7 td-col-md-6 td-col-lg-5">
                    <h3><a href="${constants.pageContext}/${article.articlePageId}" class="td-link-standalone td-link-nounderline">${article.title}</a></h3>
                    <div class="td-margin-top article-pubdate">${article.dateLabel} <span>${article.date}</span></div>
                    <p class="hidden-xs">${article.description}</p>
                    <div class="clearfix visible-lg-block"></div>
                </div>
            </li>