package com.tdsecurities.common.dto;

import java.util.List;

public class ArticleListDTO {

    private String title;
    private String loadMoreText;
    private List<ArticleDTO> articles;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLoadMoreText() {
        return loadMoreText;
    }

    public void setLoadMoreText(String loadMoreText) {
        this.loadMoreText = loadMoreText;
    }

    public List<ArticleDTO> getArticles() {
        return articles;
    }

    public void setArticles(List<ArticleDTO> articles) {
        this.articles = articles;
    }

    @Override
    public String toString() {
        return "ArticleListDTO [articles=" + articles + ", loadMoreText=" + loadMoreText + ", title="
                + title + "]";
    }

}