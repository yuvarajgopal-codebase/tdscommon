package com.tdsecurities.common.exception;

public class TemplateNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public TemplateNotFoundException() {
    }

    public TemplateNotFoundException(String message) {
        super(message);
    }

    public TemplateNotFoundException(Throwable cause) {
        super(cause);
    }

    public TemplateNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    
}