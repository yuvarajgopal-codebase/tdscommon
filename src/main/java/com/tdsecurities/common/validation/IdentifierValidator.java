package com.tdsecurities.common.validation;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IdentifierValidator implements ConstraintValidator<IdentifierConstraint, String> {

    static Pattern pattern = Pattern.compile("^[a-zA-Z0-9]+$");

    @Override
    public void initialize(IdentifierConstraint constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String identifier, ConstraintValidatorContext ctx) {
        return pattern.matcher(identifier).matches();
    }

}