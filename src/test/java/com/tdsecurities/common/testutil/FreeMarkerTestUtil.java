package com.tdsecurities.common.testutil;

import freemarker.template.Configuration;

public class FreeMarkerTestUtil {

    public static Configuration getTestFreeMarkerConfiguration() {

        Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
        configuration.setClassForTemplateLoading(FreeMarkerTestUtil.class, "/templates");
        configuration.setDefaultEncoding("UTF-8");
        return configuration;
    }
}