package com.tdsecurities.common.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.Configuration;

public class SimpleText implements BaseComponent {

    private static final Logger logger = LoggerFactory.getLogger(SimpleText.class);

    private String text;

    public SimpleText(String text) {
        this.text = text;
    }

    @Override
    public String generateContent(Configuration freeMarkerConfiguration) {

        return text;
	}

}