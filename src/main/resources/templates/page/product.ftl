    <#if imageCopy?has_content>
    ${imageCopy}
    </#if>

    <#if tabText?has_content>
        <div class="alternate-bg-color-sections">
        ${tabText}
        </div>
    </#if>
    
    <#--  <#if fromLocalToGlobal?has_content>
        <div class="alternate-bg-color-sections">
        ${fromLocalToGlobal}
        </div>
    </#if>

    <#if articleList?has_content>
        <div class="alternate-bg-color-sections">
        ${articleList}
        </div>
    </#if>   -->
