package com.tdsecurities.common.support;

public class StandardResponse {

    private String message = "Success";

    public StandardResponse() {
    }
    
    public StandardResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}