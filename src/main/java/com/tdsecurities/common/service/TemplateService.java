package com.tdsecurities.common.service;

import java.util.ArrayList;
import java.util.List;

import com.tdsecurities.common.data.TemplateInfo;
import com.tdsecurities.common.data.TemplateInfoRepository;
import com.tdsecurities.common.dto.TemplateInfoDTO;
import com.tdsecurities.common.util.TransformUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TemplateService {

    private static final Logger logger = LoggerFactory.getLogger(TemplateService.class);

    @Autowired
    TemplateInfoRepository templateInfoRepository;

    public List<TemplateInfoDTO> getTemplates() {
        
        logger.info("Getting list of TemplateInfo");

        List<TemplateInfoDTO> templateInfoResultList = new ArrayList<TemplateInfoDTO>();
        
        try {
            List<TemplateInfo> templateInfoList = templateInfoRepository.findAll();

            logger.info("Found {} TemplateInfo", (templateInfoList != null ? templateInfoList.size(): 0));

            for (TemplateInfo templateInfo: templateInfoList) {
                TemplateInfoDTO templateInfoDTO = TransformUtils.xformTemplateInfo(templateInfo);

                if (templateInfoDTO != null) {
                    templateInfoResultList.add(templateInfoDTO);
                }
            }

            return templateInfoResultList;

        } catch (Exception e) {
            logger.error("Error getting list of templated", e);
            throw e;
        }   

    }
}