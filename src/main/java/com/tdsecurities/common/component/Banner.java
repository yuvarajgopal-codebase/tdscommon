package com.tdsecurities.common.component;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import com.tdsecurities.common.config.CommonConstants;
import com.tdsecurities.common.dto.BannerDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class Banner implements BaseComponent {

    private static final Logger logger = LoggerFactory.getLogger(Banner.class);

    private BannerDTO bannerDTO;
    private CommonConstants commonConstants;

    public Banner(BannerDTO bannerDTO, CommonConstants commonConstants) {
        this.bannerDTO = bannerDTO;
        this.commonConstants = commonConstants;
    }

    @Override
    public String generateContent(Configuration freeMarkerConfiguration) {

        try {

            Map<String, Object> templateParams = new HashMap<>();

            Template template = freeMarkerConfiguration.getTemplate("component/banner.ftl");
            templateParams.put("banner", bannerDTO);
            templateParams.put("constants", commonConstants);

            StringWriter stringWriter = new StringWriter();
            template.process(templateParams, stringWriter);

            return stringWriter.toString();

        } catch (Exception e) {
            logger.error("Error generating banner content: ", e);
            throw new RuntimeException("Error generating banner content", e);
        }
        
	}

}