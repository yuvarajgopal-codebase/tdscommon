package com.tdsecurities.common.data;

public enum ImageSize {
    xs,
    sm,
    md,
    lg
}