package com.tdsecurities.common.exception;

public class ItemExistsException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ItemExistsException() {
    }

    public ItemExistsException(String message) {
        super(message);
    }

    public ItemExistsException(Throwable cause) {
        super(cause);
    }

    public ItemExistsException(String message, Throwable cause) {
        super(message, cause);
    }



}