        <!--Start Text with Buttons-->
        <#if textWithButton?has_content>

        <!-- TODO: Change td-subscribe-now-button style to something more generic -->
        <section class="td-text-with-buttons td-subscribe-now-button">
            <div class="td-container">
                
                <!-- Start Optional Field:: 'Header' -->
                <#if textWithButton.title?has_content>
                <div class="td-row">
                    <div class="td-col-xs-12 td-col-sm-8 td-col-sm-offset-2 td-col-md-6 td-col-md-offset-3 ">
                        <div class="rte">
                            <h2 class="td-rte-margin-top-none td-rte-margin-bottom-large">${textWithButton.title}</h2>
                        </div>
                    </div>
                </div>
                </#if>
                <!-- End Optional Field -->
                
                <!-- Start Optional Field:: 'Button' -->
                <#if textWithButton.button?has_content>
                <div class="td-row">
                    <div class="td-col-xs-12 btn_cta large">
                        <button onclick="window.location.href='${constants.pageContext}/${textWithButton.button.pageId}'" class="td-button td-button-block td-button-clear-green">
                            <span class="td-icon"></span>${textWithButton.button.description}
                        </button>
                    </div>		
                </div>
                </#if>
                <!-- End Optional Field -->
                
            </div>
        </section>

        </#if>
        <!--End Text with Buttons -->
