package com.tdsecurities.common.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PageDataRepository extends CrudRepository<PageData, Long> {

    List<PageData> findByPageIdAndPageVersionAndLanguage(String pageId, long pageVersion, String language);
    List<PageData> findByPageIdAndPageVersion(String pageId, long pageVersion);
}