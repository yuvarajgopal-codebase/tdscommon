package com.tdsecurities.common.component;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tdsecurities.common.config.CommonConstants;
import com.tdsecurities.common.dto.ProfileDTO;
import com.tdsecurities.common.dto.LeadershipGridDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class LeadershipGrid extends CompositeComponent<Profile> implements BaseComponent {

    private static final Logger logger = LoggerFactory.getLogger(LeadershipGrid.class);

    private LeadershipGridDTO leadershipGridDTO;
    private CommonConstants commonConstants;

    public LeadershipGrid(LeadershipGridDTO leadershipGridDTO, CommonConstants commonConstants) {
        this.leadershipGridDTO = leadershipGridDTO;
        this.commonConstants = commonConstants;
    }

    @Override
    public String generateContent(Configuration freeMarkerConfiguration) {

        try {
            if (leadershipGridDTO == null) {
                return null;
            }

            Map<String, Object> templateParams = new HashMap<>();

            Template template = freeMarkerConfiguration.getTemplate("component/leadership_grid.ftl");
            templateParams.put("title", leadershipGridDTO.getTitle());
            templateParams.put("subtitle", leadershipGridDTO.getSubtitle());

            if (leadershipGridDTO.getProfiles() != null) {
                for (ProfileDTO profileDTO: leadershipGridDTO.getProfiles()) {
                    add(new Profile(profileDTO, commonConstants));
                }
            }

            List<String> profileContentList = getGeneratedComponentContent(freeMarkerConfiguration);
            templateParams.put("profiles", profileContentList);

            StringWriter stringWriter = new StringWriter();
            template.process(templateParams, stringWriter);

            return stringWriter.toString();

        } catch (Exception e) {
            logger.error("Error generating leadership grid content: ", e);
            throw new RuntimeException("Error generating leadership grid content", e);
        }

	}
}