        <!--Start Footer-->
		<footer role="contentinfo" class="td-fullwidth-dark-green td-padding-vert-0">
            <div class="td-container">
                <div class="td-row">
                    <div class="td-footer-content td-col-xs-12" style="background-image:url('assets/img/footer/footer_seat_tcm380-268665.png');">
                        <p class="td-footer-heading td-copy-white td-copy-align-centre">Need to talk to us directly?<a href="contact-us.html" class="td-contact-link td-link-nounderline td-link-standalone">Contact us</a></p>
                        <div class="td-footer-social td-copy-align-centre">
                            <p class="td-footer-social-heading">Connect with TD Securities</p>
                            <ul>
                                <li>
                                    <a class="td-link-nounderline" class="td-icon-link" href="http://www.youtube.com" target="_blank">
                                        <div class="td-icon-wrapper td-interactive-icon td-background-darkgreen">
                                            <span aria-hidden="true" class="td-icon td-icon-youtubeLogo"></span>
                                            <span class="td-forscreenreader" id="icon3Description">YouTube</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a class="td-link-nounderline" href="https://www.linkedin.com" target="_blank">
                                        <div class="td-icon-wrapper td-interactive-icon td-background-darkgreen">
                                            <span aria-hidden="true" class="td-icon td-icon-linkedin"></span>
                                            <span class="td-forscreenreader" id="icon6Description">LinkedIn</span>
                                        </div>
                                    </a>
                                </li>						
                            </ul>
                        </div>
                        
                        <div class="td-footer-links">
                                <a class="td-copy-white td-link-nounderline td-copy-standard" target="_blank" href="#">Privacys & Security</a>
                                <a class="td-copy-white td-link-nounderline td-copy-standard" target="_blank" href="#">Legal</a>
                                <a class="td-copy-white td-link-nounderline td-copy-standard" target="_blank" href="#">Accessibility</a>
                                <a class="td-copy-white td-link-nounderline td-copy-standard" target="_blank" href="#">Regulatory</a>
                                <a class="td-copy-white td-link-nounderline td-copy-standard" target="_blank" href="#">Terms of Use</a>
                                <a class="td-copy-white td-link-nounderline td-copy-standard" target="_blank" href="#">Site Index</a>

                        </div>
                    </div>

                </div>
                
                <!-- Custom Footer for IIROC and CIPF Logos Begins -->
		
            <div class="td-row rte">
                <div class="td-footer-content td-col-xs-12" style="padding-top:0;padding-bottom:0;">
                    <div class="td-footer-social td-row">
                        <div class="td-col-sm-12 td-col-md-3 td-col-md-offset-3">
                            <a href="http://www.iiroc.ca" target="_blank"><img src="https://www.td.com/ca/images/emerald/img/IIROC_en_desktop.png" alt="" />
                            </a>
                        </div>
                        <div class="td-col-sm-12 td-col-md-3">
                            <br/>
                            <a href="https://www.cipf.ca" target="_blank"><img src="https://www.td.com/ca/images/emerald/img/CIPF_en_desktop.png" class="td-rte-margin-top-none" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>	
            
		
		<!-- Custom Footer for IIROC and CIPF Logos Ends -->
            </div>   
        </footer>
        <!--End Footer-->