	<div class="logo-print visible-print" aria-hidden="true">
        <img src="/assets/img/header-nav/TDB_white.png" width="36" height="32" alt="TD Canada Trust">
    </div>

    <div class="td_rq_header-nav td-header-nav">
            <!-- Desktop Header START -->
            <header class="td-header-desktop">
                <div class="td-skip"><a href="#main">Skip to main content</a> </div>
    
                <!-- Desktop utility toggle START -->
                <div class="td-utility-toggle">
                    <div class="td-container">
                        <div class="td-section-left">
                            <div class="td-segments">
                                <ul>
                                    <li><a href="">Other TD Sites</a></li>
                                    <li><a href="">Our Offices</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="td-section-right">
                            <div class="td-other-toggles">
                                <ul>
                                    <li class="td-dropdown td-dropdown-country">
                                        <a href="#"><span class="td-forscreenreader">Select country</span><img class="country-flag" src="/assets/img/header-nav/country_ca.png" alt="Canada" /><span class="td-icon expand" aria-hidden="true"></span><span class="td-icon collapse" aria-hidden="true"></span></a>
                                        <ul class="td-dropdown-content">
                                            <li class="active"><a href="#"><img class="country-flag" src="/assets/img/header-nav/country_ca.png" alt="" />Canada<span class="td-icon selected" aria-hidden="true"></span><span class="td-forscreenreader">Selected</span></a></li>
                                            <li><a href="#"><img class="country-flag" src="/assets/img/header-nav/country_us.png" alt="" />United States</a></li>
                                        </ul>
                                    </li><!--
                                    --><li class="td-dropdown td-dropdown-language">
                                        <a href="#"><span class="td-forscreenreader">Select language</span>English<span class="td-icon expand" aria-hidden="true"></span><span class="td-icon collapse" aria-hidden="true"></span></a>
                                        <ul class="td-dropdown-content">
                                            <li class="active"><a href="#">English<span class="td-icon selected" aria-hidden="true"></span><span class="td-forscreenreader">Selected</span></a></li>
                                            <li><a href="#">Français</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Desktop utility toggle END -->
    
                <!-- Desktop primary nav START -->
                <div class="td-nav-primary">
                    <div class="td-container">
                        <div class="td-section-left">
                                <div class="td-logo">
                                    <a href="/">
                                        <img src="/assets/img/header-nav/1200px-TD_Canada_Trust_logo.png" srcset="" alt="TD Canada Trust" />
                                    </a>  
                                </div>
                                <div  class="tds-security-logo">
                                        <a href="/">
                                            <img src="/assets/img/header-nav/td_e_secur_h_p_g_rgb.png"  srcset="" alt="TD Canada Trust"  width="200px"/>
                                        </a>
                                </div>
                            <nav>
                                <ul>
                                    <li class="td-dropdown">
                                        <a href="#">About Us<span class="td-icon expand" aria-hidden="true"></span><span class="td-icon collapse" aria-hidden="true"></span><span class="td-forscreenreader">Selected</span></a>
                                            <ul class="td-dropdown-content">
                                                <li><a href="#">Overview</a></li>
                                                <li><a href="#">Awards</a></li>
                                                <li><a href="#">Events</a></li>
                                                <li><a href="#">Deals</a></li>
                                                <li><a href="#">Diversity & Inclusion</a></li>
                                                <li><a href="#">Leadership</a></li>
                                                <li><a href="#">History</a></li>
                                                <li><a href="#">Media & Investor Relations</a></li>
                                            </ul>
                                    </li>
                                    <li class="td-dropdown ">
                                        <a href="#">Services<span class="td-icon expand" aria-hidden="true"></span><span class="td-icon collapse" aria-hidden="true"></span><span class="td-forscreenreader">Selected</span></a>
                                            <ul class="td-dropdown-content">
                                                <li><a href="#">Global Investment Banking</a></li>
                                                <li><a href="#">Global Markets</a></li>
                                                <li><a href="#">Global Transaction Banking</a></li>
                                                <li><a href="#">Research & Strategy</a></li>
                                            </ul>
                                    </li>
                                    <li class="td-dropdown">
                                        <a href="#">Insights</a>
                                    </li>
                                    <li class="td-dropdown">
                                        <a href="#">Community<span class="td-icon expand" aria-hidden="true"></span><span class="td-icon collapse" aria-hidden="true"></span><span class="td-forscreenreader">Selected</span></a>
                                        <ul class="td-dropdown-content">
                                            <li><a href="#">Our Commitment</a></li>
                                            <li><a href="#">Underwriting Hope</a></li>
                                            <li><a href="#">Employee giving</a></li>
                                        </ul>
                                    </li>
                                    <li class="td-dropdown">
                                        <a href="#">Careers</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
    
                        <div class="td-section-right">
                            <div class="td-quick-access">
                                    <ul>
                                        <li class="search"><a href="#" class="td-desktop-search-show-btn"><span class="td-icon"></span><span class="td-label">Search</span></a></li>
                                        <li class="login td-us-public-login td-dropdown">
                                            <a href="#" class="td-show-label"><span class="td-icon"></span><span class="td-label">Login</span><span class="td-icon expand" aria-hidden="true"></span><span class="td-icon collapse" aria-hidden="true"></span></a>
                                            <div class="td-dropdown-content">
                                                <h2>Log in to your other accounts</h2>
                                                <div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                            </div>
                        </div>
                        <div class="td-nav-desktop-search">
                            <div class="td-search-container">
                                <form class="td-search-box">
                                    <input type="text" placeholder="Search" name="search" class="td-search-input">
                                    <span class="td-search-icon" aria-hidden="true"><span class="td-icon"></span></span>
                                    <input type="submit" class="td-search-submit" value="search">
                                </form>
                                <button type="button" class="td-desktop-search-hide-btn"><span class="td-forscreenreader">Close Search</span><span class="td-icon" aria-hidden="true"></span></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Desktop primary nav END -->
            </header>
            <!-- Desktop Header END -->
    
            <!-- Mobile Header START -->
            <header class="td-header-mobile">
                <div class="td-container">
                    <div class="td-section-left">
                        <button type="button" class="td-mobile-action-button td-mobile-menu-button">
                            <div class="td-mobile-menu-button-icon">
                                <span class="td-forscreenreader">Open menu</span>
                                <span class="icon-bar" aria-hidden="true"></span>
                                <span class="icon-bar" aria-hidden="true"></span>
                                <span class="icon-bar" aria-hidden="true"></span>
                            </div>
                            <div class="td-logo"><img src="/assets/img/header-nav/td-logo.png" srcset="" alt="TD Canada Trust" /></div>
                        </button>
                        <button type="button" class="td-mobile-action-button td-mobile-back-button" onclick="history.back();">
                            <div class="td-mobile-back-button-icon"><span class="td-icon"></span><span class="td-forscreenreader">Back</span></div>
                                <div class="td-logo"><img src="/assets/img/header-nav/td-logo.png" srcset="" alt="TD Canada Trust" /></div>
                        </button>
                    </div>
                    <div class="td-section-right">
                        <button type="button" class="td-mobile-action-button td-mobile-menu-secondary-button td-login">
                            <span class="td-icon login"></span><span class="label">Login</span>
                        </button>
                    </div>
                </div>
            </header>
            <!-- Mobile Header END -->
    
            <!-- Mobile Off-canvas Menu START -->
            <div class="td-nav-mobile">
                <!-- Primary mobile menu START -->
                <div class="td-nav-mobile-menu td-nav-mobile-menu-primary">
                    <div class="td-nav-mobile-menu-top">
                        <div class="td-nav-mobile-menu-header">
                            <div class="td-logo"><a href="/"><img src="/assets/img/header-nav/td-logo.png" srcset="" alt="TD Canada Trust" /></a></div>
                            <button type="button" class="td-mobile-menu-close"><span class="td-forscreenreader" aria-hidden="">Close Menu</span><span class="td-icon" aria-hidden="true"></span></button>
                        </div>
                        <div class="td-nav-mobile-menu-search">
                            <form class="td-search-box">
                                <input type="text" placeholder="Search" name="search" class="td-search-input">
                                <span class="td-search-icon"><span class="td-icon" aria-hidden="true"></span></span>
                                <input type="submit" class="td-search-submit" value="search">
                            </form>
                        </div>
                    </div>
                    <nav>
                        <ul class="td-nav-mobile-menu-list">
                            <li class="td-nav-mobile-menu-item td-item-nav-link item-home ">
                                <a href="#"><span class="td-icon td-icon-homepage"></span>Our Offices</a>
                            </li>
                            <li class="td-nav-mobile-menu-item td-item-nav-link item-home ">
                                <a href="#"><span class="td-icon td-icon-homepage"></span>Home</a>
                            </li>
                            <li class="td-nav-mobile-menu-item td-item-nav-link td-accordion">
                                <a href="#"><span class="td-icon td-icon-solutions"></span>About Us<span class="td-icon expand"></span><span class="td-icon collapse"></span></a>
                                <ul class="td-accordion-content">
                                    <li><a href="#">Overview</a></li>
                                    <li><a href="#">Awards</a></li>
                                    <li><a href="#">Events</a></li>
                                    <li><a href="#">Deals</a></li>
                                    <li><a href="#">Diversity & Inclusion</a></li>
                                    <li><a href="#">Leadership</a></li>
                                    <li><a href="#">History</a></li>
                                    <li><a href="#">Media & Investor Relations</a></li>       
                                </ul>
                            </li>
                            <li class="td-nav-mobile-menu-item td-item-nav-link item-home">
                                <a href="#"><span class="td-icon td-icon-homepage"></span>Global Investment Banking</a>
                            </li>
                            <li class="td-nav-mobile-menu-item td-item-nav-link item-home">
                                <a href="#"><span class="td-icon td-icon-homepage"></span>Global Markets</a>
                            </li>
                            <li class="td-nav-mobile-menu-item td-item-nav-link item-home">
                                <a href="#"><span class="td-icon td-icon-homepage"></span>Global Transaction Banking</a>
                            </li>
                            <li class="td-nav-mobile-menu-item td-item-nav-link item-home">
                                <a href="#"><span class="td-icon td-icon-homepage"></span>Research & Strategy</a>
                            </li>
                            <li class="td-nav-mobile-menu-item td-item-nav-link td-accordion">
                                <a href="#"><span class="td-icon td-icon-solutions"></span>Insights</a>
                            </li>
                            <li class="td-nav-mobile-menu-item td-item-nav-link td-accordion">
                                <a href="#"><span class="td-icon td-icon-solutions"></span>Community<span class="td-icon expand"></span><span class="td-icon collapse"></span></a>
                                <ul class="td-accordion-content">
                                    <li><a href="#">Our Commitment</a></li>
                                    <li><a href="#">Underwriting Hope</a></li>
                                    <li><a href="#">Employee giving</a></li>
                                </ul>
                            </li>
                            <li class="td-nav-mobile-menu-item td-item-nav-link td-accordion ">
                                <a href="#"><span class="td-icon td-icon-careers"></span>Careers</a>
                            </li>
                            
                            <li class="td-nav-mobile-menu-item td-item-toggle td-accordion td-accordion-country">
                                <a href="#">Country: Canada<span class="td-icon expand"></span><span class="td-icon collapse"></span></a>
                                <ul class="td-accordion-content">
                                    <li class="active"><a href="#"><img class="country-flag" src="/assets/img/header-nav/country_ca.png" alt="Canada" />Canada<span class="td-icon selected" aria-hidden="true"></span><span class="td-forscreenreader">Selected</span></a></li>
                                    <li><a href="#"><img class="country-flag" src="/assets/img/header-nav/country_us.png" alt="Canada" />United States</a></li>
                                </ul>
                            </li>
                            <li class="td-nav-mobile-menu-item td-item-toggle td-accordion td-accordion-language">
                                <a href="#">Language: English<span class="td-icon expand"></span><span class="td-icon collapse"></span></a>
                                <ul class="td-accordion-content">
                                    <li class="active"><a href="#">English<span class="td-icon selected" aria-hidden="true"></span><span class="td-forscreenreader">Selected</span></a></li>
                                    <li><a href="#">Français</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- Primary mobile menu END -->
    
                <!-- Secondary mobile menu START -->
                <div class="td-nav-mobile-menu td-nav-mobile-menu-secondary td-nav-mobile-menu-right">
                    <div class="td-nav-mobile-menu-top">
                        <div class="td-nav-mobile-menu-header">
                            <div class="td-nav-mobile-menu-title"><span class="td-icon login" aria-hidden="true"></span>Login</div>
                            <button type="button" class="td-mobile-menu-close"><span class="td-forscreenreader" aria-hidden="">Close Menu</span><span class="td-icon" aria-hidden="true"></span></button>
                        </div>
                    </div>
                    <nav>
                        <ul class="td-nav-mobile-menu-list">
                            <li class="td-nav-mobile-menu-item td-item-nav-link">
                                <h3>Log in to your other accounts</h3>
                            </li>										
                        </ul>
                    </nav>
                </div>
                <!-- Secondary mobile menu END -->
                <div class="td-nav-mobile-overlay"></div>
            </div>
            <!-- Mobile Off-canvas Menu END -->
    </div>

    <div class="td_rq_spinner td-spinner" aria-hidden="true">
        <img src="/assets/img/spinner/loading-spinner-small.gif" alt="loading">
    </div>