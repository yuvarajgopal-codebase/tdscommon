package com.tdsecurities.common.dto;

public class TabTextItemDTO {

    private String tabTitle;
    private String title;
    private String imageId;
    private String description;
    private String textBlock;

    private String bottomTitle;
    private String bottomTextLeft;
    private String bottomTextRight;

    
    @Override
    public String toString() {
        return "ImageCopyDTO [ description=" + description + ", imageId=" + imageId + ", title=" + title + "]";
    }

    /**
     * @return the tabTitle
     */
    public String getTabTitle() {
        return tabTitle;
    }

    /**
     * @param tabTitle the tabTitle to set
     */
    public void setTabTitle(String tabTitle) {
        this.tabTitle = tabTitle;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the imageId
     */
    public String getImageId() {
        return imageId;
    }

    /**
     * @param imageId the imageId to set
     */
    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the textBlock
     */
    public String getTextBlock() {
        return textBlock;
    }

    /**
     * @param textBlock the textBlock to set
     */
    public void setTextBlock(String textBlock) {
        this.textBlock = textBlock;
    }

    /**
     * @return the bottomTitle
     */
    public String getBottomTitle() {
        return bottomTitle;
    }

    /**
     * @param bottomTitle the bottomTitle to set
     */
    public void setBottomTitle(String bottomTitle) {
        this.bottomTitle = bottomTitle;
    }

    /**
     * @return the bottomTextLeft
     */
    public String getBottomTextLeft() {
        return bottomTextLeft;
    }

    /**
     * @param bottomTextLeft the bottomTextLeft to set
     */
    public void setBottomTextLeft(String bottomTextLeft) {
        this.bottomTextLeft = bottomTextLeft;
    }

    /**
     * @return the bottomTextRight
     */
    public String getBottomTextRight() {
        return bottomTextRight;
    }

    /**
     * @param bottomTextRight the bottomTextRight to set
     */
    public void setBottomTextRight(String bottomTextRight) {
        this.bottomTextRight = bottomTextRight;
    }

}